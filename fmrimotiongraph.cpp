/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file fmrimotiongraph.cpp
  \author Colin Humphries
  \brief Fmrimotiongraph main function.
*/

#include <iostream>
#include <fstream>
#include <string>
#include "argparser.h"
#include "mrivol.h"
#include "niftifile.h"
#include "envdefaults.h"
#include "mrirotate.h"
#include "svglinegraph.h"
#include "version.h"
#include "helptext.h"

/*!
  \fn print_help
  \brief Print help message.

  This function prints a help message to stdout.
 */
void print_help(bool verbose, bool isshort) {
  using namespace std;
  using namespace argparser;
  HelpText ht;
  ostringstream ss(ios::ate);
  ht.SetFunctionName("fmrimgraph");
  ht.SetFunctionDescription("Plot the relative position of a voxel over time");
  ht.SetFunctionVersion(MARKER_VERSION_STRING);
  
  ht.AddArgument("mat","Rotation matrix file(s)",-1);
  ht.AddDescription("-mat <filename(s)...>",FM_HELPTEXT_MATOPT);
  
  ht.AddArgument("out","Output graph (SVG format)",1);
  ht.AddDescription("-out <filename>","The output is a graph in SVG format of the position at each time point of the voxel relative to the first time point.");

  ht.AddArgument("pkg","Software package (afni,fsl)",1);
  ht.AddDescription("-pkg <packagename>","The software used to create the rotation matrices, which defines the coordinate system that was used. Valid options include: afni, fsl, nifti. The nifti option is used to specify a generic NIFTI coordinate system.");
  
  ht.AddArgument("vox","Voxel i,j,k indices",3);
  ht.AddDescription("-vox <num> <num> <num>","Indices (i,j,k) of the voxel of interest in the data volume.");

  ht.AddArgument("vox","Voxel index",1);
  ht.AddDescription("-vox <num>","Offset (index) of the voxel in the data volume.\n* Note that for a 64x64x32 volume, offset = i + j*64 + k*64*64 where i,j,k are the volume indices of the voxel.");

  ht.AddArgument("pos","Voxel x, y, z position",3);
  ht.AddDescription("-pos <num> <num> <num>","Position (x,y,z mm) of the voxel of interest.");

  ht.AddArgument("voxsrc","Voxel source volume (orig,ref) (default = ref)",1);
  ht.AddDescription("-voxsrc <orig/ref>","The voxel or position can be defined based on either the original data volume or the reference volume.");

  ht.AddArgument("ref","Reference volume",1);
  ht.AddArgument("orig","Original volume",1);
  vector<string> atmp;
  atmp.push_back("-ref <filename>");
  atmp.push_back("-orig <filename>");
  ht.AddDescription(atmp,"* If using the '-vox' option then either the reference or original data volume should be specified using either '-ref' or '-orig' depending on whether '-voxsrc' is set to 'orig' or 'ref'.\n* If using FSL then both '-ref' and '-orig' should be specified.");

  ss.str("Display additional information: on/off (default = ");
  if (verbose) {
    ss << "on" << ")";
  }
  else {
    ss << "off" << ")";
  }
  ht.AddArgument("verbose",ss.str(),1);
  ht.AddArgument("help","Print a detailed help message.",0);

  if (isshort) {
    cout << ht.ShortText() << endl;
  }
  else {
    cout << ht.LongText() << endl;
  }

}

/*!
  \fn main
  \brief Main function

  This is the main function for fmrimotion
 */
int main (int argc, const char **argv) {
  using namespace std;
  using namespace argparser;
  using namespace fmri;
  
  bool debug = false;
  bool verbose = false;
  string matpkg = "afni";
  string reffile, origfile, outfile, matfile;
  bool voxisref = true;

  // Load configuration settings /////////////////////////////////////
  EnvDefaults envd;
  envd.SetPackageName("fmrimarker");
  envd.AddVariable("FMRIMARKER_MAT_PACKAGE","AFNI");
  envd.AddVariable("FMRIMARKER_VERBOSE","NO");

  // Get values from environmental variables and config files
  envd.CheckSystem();
  envd.PullBool("FMRIMARKER_VERBOSE",verbose);
  envd.PullLower("FMRIMARKER_MAT_PACKAGE",matpkg);

  // Parse command line options /////////////////////////////////////
  if (argc < 2) {
    print_help(verbose,true);
    return 0;
  }
  ArgParser parg(argv,argc);
  if (parg.Exists("help") || parg.Exists("h")) {
    print_help(verbose,false);
    return 0;
  }
  if (parg.Exists("debug")) {
    debug = true;
  }

  // parg.Get("mat",matfile);
  // if (!parg.Found() || parg.Error()) {
  //   cerr << "Error: rotation matrix file not specified" << endl;
  //   return 1;
  // }

  parg.Get("out",outfile);
  if (!parg.Found() || parg.Error()) {
    cerr << "Error: outfile not specified" << endl;
    return 1;
  }

  parg.Get("ref",reffile);
  if (parg.Error()) {
    cerr << "Error reading -ref option" << endl;
    return 1;
  }
  parg.Get("orig",origfile);
  if (parg.Error()) {
    cerr << "Error reading -orig option" << endl;
    return 1;
  }
  string vtmp;
  parg.Get("voxsrc",vtmp);
  if (parg.Error()) {
    cerr << "Error reading -voxsrc option" << endl;
    return 1;
  }
  if (vtmp.compare("orig") == 0) {
    voxisref = false;
  }
  else if (vtmp.compare("ref") == 0) {
    voxisref = true;
  }
  else {
    cerr << "Error: unknown value for -voxsrc" << endl;
    return 1;
  }
  parg.Get("verbose",verbose);
  if (parg.Error()) {
    cerr << "Error parsing -verbose option" << endl;
    return 1;
  }
  if (debug) {
    verbose = true;
  }

  parg.Get("pkg",matpkg);
  // if pkg=fsl then both orig and ref should be defined
  // otherwise orig should be defined if -voxsrc orig and -vox option
  // and ref should be defined if -voxsrc ref and -vox option

  // Load rotation file
  MRIRotate mrot;
  NIFTIFile nin;

  if (matpkg.compare("afni") == 0) {
    mrot.SetInSpace(RotationSpace::DICOM);
    mrot.ApplyInverse(true);
  }
  else if (matpkg.compare("fsl") == 0) {
    mrot.SetInSpace(RotationSpace::FSL);
    MRIBase source;
    MRIBase target;
    string afile1;
    parg.Get("orig",afile1);
    if (parg.Found()) {
      if (!nin.Open(afile1,'r')) {
	cerr << "Error: opening orig file" << endl;
	return 1;
      }
      nin.Read(source);
      nin.Close();
    }
    parg.Get("ref",afile1);
    if (parg.Found()) {
      if (!nin.Open(afile1,'r')) {
	cerr << "Error opening ref file" << endl;
	return 1;
      }
      nin.Read(target);
      nin.Close();
    }
    mrot.SetInSpaceFSL(source,target);
  }
  else if (matpkg.compare("nifti") == 0) {
    mrot.SetInSpace(RotationSpace::NIFTI);
  }
  else {
    cerr << "Error: unknown option for -pkg: " << matpkg << endl;
    return 1;
  }
  
  // Load rotation matrices //////////////////////////////////
  vector<OrientationMatrix> matdata;
  vector<string> matfiles;
  parg.Get("mat",matfiles);
  if (parg.Found()) {
    mrot.ReadMAT(matfiles,matdata);
    matfile = matfiles[0];
    if (!voxisref) {
      OrientationMatrix omat = matdata[0];
      omat.Invert();
      for (vector<OrientationMatrix>::iterator it=matdata.begin(); it != matdata.end(); ++it) {
	*it = omat * (*it);
      }
    }
  }
  else {
    parg.Get("fsldir",matfile);
    if (parg.Found()) {
      mrot.ReadFSLDir(matfile,matdata);
    }
    else {
      cerr << "Error: No rotation file/directory specified" << endl;
      return 1;
    }
  }
  
  vector<double> xdata, ydata, zdata, vdata;
  double xp, yp, zp, nxp, nyp, nzp, nxp1, nyp1, nzp1;
  // string tstr = "Relative Motion";
  ostringstream tss;
  tss << "Relative Motion";
  if (parg.Exists("vox")) {
    MRIBase base;
    string afile1;
    if (voxisref) {
      if (reffile.empty()) {
	cerr << "Error: -ref must be specified when using the -vox option" << endl;
	return 1;
      }
      afile1 = reffile;
    }
    else {
      if (origfile.empty()) {
	cerr << "Error: -orig must be specified when using the -vox option" << endl;
	return 1;
      }
      afile1 = origfile;
    }
    if (!nin.Open(afile1,'r')) {
      cerr << "Error: opening orig/ref file" << endl;
      return 1;
    }
    nin.Read(base);
    nin.Close();
    vector<uint> vinds;
    parg.Get("vox",vinds);
    tss << " - Voxel: ";
    if (vinds.size() == 1) {
      base.Vox2Pos(vinds[0],xp,yp,zp);
      tss << vinds[0];
      if (verbose) {
	cout << "Voxel index: " << vinds[0] << endl;
      }
    }
    else if (vinds.size() == 3) {
      base.Vox2Pos(vinds[0],vinds[1],vinds[2],xp,yp,zp);
      tss << vinds[0] << ", " << vinds[1] << ", " << vinds[2];
      if (verbose) {
	cout << "Voxel indices: " << vinds[0] << ", " << vinds[1] << ", " << vinds[2] << endl;
      }
    }
    else {
      cerr << "Error parsing -vox argument" << endl;
      return 1;
    }

  }
  else if (parg.Exists("pos")) {
    parg.Get("pos",0,xp);
    parg.Get("pos",1,yp);
    parg.Get("pos",2,zp);
    if (parg.Error()) {
      cerr << "Error reading -pos argument" << endl;
      return 1;
    }
  }
  else {
    cerr << "Error: neither -vox nor -pos is defined" << endl;
    return 1;
  }
  tss << " - Position: ";
  tss << xp << ", " << yp << ", " << zp;
  if (verbose) {
    cout << "Original voxel position: " << xp << ", " << yp << ", " << zp << endl;
  }
  // xp = 0.0;
  // yp = 10.0;
  // zp = 0.0;
  for (vector<OrientationMatrix>::iterator it = matdata.begin();
       it != matdata.end(); ++it) {
    it->Transform(xp,yp,zp,nxp,nyp,nzp);
    if (it == matdata.begin()) {
      nxp1 = nxp;
      nyp1 = nyp;
      nzp1 = nzp;
    }
    // cout << nxp << " " << nyp << " " << nzp << endl;
    xdata.push_back(nxp-nxp1);
    ydata.push_back(nyp-nyp1);
    zdata.push_back(nzp-nzp1);
  }

  xmlgen::SVGTag maingraph;
  maingraph.setWidth(600);
  maingraph.setHeight(400);

  SVGLineGraph *sgraph = new SVGLineGraph();
  maingraph.addGElement(sgraph);
  sgraph->addLineData(xdata);
  sgraph->addLineData(ydata);
  sgraph->addLineData(zdata);

  sgraph->setXLabel("Volume Number");
  sgraph->setYLabel("Distance (mm)");
  sgraph->setTitle(tss.str());
  sgraph->setWidth(600);
  sgraph->setHeight(350);
  sgraph->setX(0.0);
  sgraph->setY(0.0);
  sgraph->Plot();
  
  xmlgen::XTag *ptag;
  maingraph.addText(420,360,"X");
  ptag = maingraph.addLine(435,354,455,354);
  ptag->addAttribute("style","stroke:rgb(0,0,255);");
  maingraph.addText(480,360,"Y");
  ptag = maingraph.addLine(495,354,515,354);
  ptag->addAttribute("style","stroke:rgb(0,180,0);");
  maingraph.addText(540,360,"Z");
  ptag = maingraph.addLine(555,354,575,354);
  ptag->addAttribute("style","stroke:rgb(255,0,0);");

  // cout << sgraph.Render() << endl;
  fstream fout;
  fout.open(outfile,fstream::out);
  if (fout.fail()) {
    cerr << "Error opening output file" << endl;
    return 1;
  }
  fout << maingraph.Render() << endl;
  fout.close();

  return 0;
}
