/*********************************************************************
* Copyright 2016, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file svglinegraph.h
  \author Colin Humphries
  \brief Classes for creating SVG documents.
*/
#ifndef _SVGLINEGRAPH_H
#define _SVGLINEGRAPH_H

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <cmath>
#include <iomanip>

#include "svggraph.h"
#include "svgbasicgraph.h"

// using namespace std;

class SVGLineGraph : public svggraph::SVGBasicGraph {
public:
  SVGLineGraph() {
    lcount = 0;
    linecolors = {"rgb(0,0,255)","rgb(0,180,0)","rgb(255,0,0)",
		  "rgb(0,255,255)","rgb(255,0,255)","rgb(239,120,0)"};
  }

  virtual std::string Render() {
    return svggraph::SVGBasicGraph::Render();
  }

  void addLineData(std::vector<double> &ydata) {
    std::vector<double> xdata;
    for (size_t ii=0; ii<ydata.size(); ++ii) {
      xdata.push_back((double)(ii+1));
    }
    svggraph::LineElement *ple;
    ple = new svggraph::LineElement();
    ple->ydata = ydata;
    ple->xdata = xdata;
    ple->color = linecolors[lcount];
    addGraphElement(ple);
    ++lcount;
    if (lcount == linecolors.size()) {
      lcount = 0;
    }
  }
  
  /*
  void Plot() {
    // PlotAxis();
    SVGGraph::Plot();
    XTag *pxt;
    for (vector<LineData>::iterator it = data.begin(); it != data.end(); ++it) {
      vector<double> newx = it->xdata;
      vector<double> newy = it->ydata;
      ConvertPnts(newx,newy);
      if (it->linestyle == LineStyle_t::SOLID) {
	pxt = addPolyLine(newx,newy);
	pxt->addAttribute("style","stroke:" + it->linecolor + ";");
      }      
      if (it->marker == Marker_t::CIRCLE) {
	for (uint ii = 0; ii < newx.size(); ++ii) {
	  pxt = addCircle(newx[ii],newy[ii],(it->markersize)/2);	  
	  if (it->markerfill) {
	    pxt->addAttribute("style","stroke:" + it->markercolor + 
			      ";fill:" + it->markercolor + ";");
	  }
	  else {
	    pxt->addAttribute("style","stroke:" + it->markercolor + ";");
	  }
	}
      }
    }

  }
  */

  
private:
  uint lcount;
  std::vector<std::string> linecolors;
};

#endif
