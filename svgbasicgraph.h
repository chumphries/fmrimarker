/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of SVGGraph.
*
* SVGGraph is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* SVGGraph is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with SVGGraph.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file svgbasicgraph.h
  \author Colin Humphries
  \brief Basic graph class.
*/
#ifndef _SVGBASICGRAPH_H
#define _SVGBASICGRAPH_H

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <cmath>
#include <iomanip>

#include "xmldoc.h"
#include "svggraph.h"

// using namespace std;

namespace svggraph {
  
  class SVGBasicGraph;
  
  enum class DrawingOrder {NONE,BELOWAXIS,BOTTOM,TOP};

  /*! \class GraphElement
    \brief Base Graphic Element Class
    
    This is the base class for defining graphics elements that
    are added to the SVGBasicGraph class objects.
    
    \author Colin Humphries
  */
  class GraphElement {
  public:
    GraphElement() {
      drawingorder = DrawingOrder::NONE;
      ongraph = true;
    }
    virtual ~GraphElement() {;}
    virtual void Draw(SVGBasicGraph *) {;}
    virtual double XMin() {return 0.0;}
    virtual double XMax() {return 0.0;}
    virtual double YMin() {return 0.0;}
    virtual double YMax() {return 0.0;}
    DrawingOrder drawingorder;
    bool ongraph;
  };

  /*! \class SVGBasicGraph
    \brief SVG Basic Graph Class
    
    This class defines a basic graph with a mechanism for adding
    generic graphic elements (lines, shapes, etc). This class
    can be used as the basis for defining more specific types
    of graphs.
    
    \author Colin Humphries
  */
  class SVGBasicGraph : public svggraph::SVGGraph {
  public:
    SVGBasicGraph() {
    }
    ~SVGBasicGraph() {
      for (std::vector<GraphElement *>::iterator it = gelements.begin();
	   it != gelements.end(); ++it) {
	delete *it;
      }
    }
    virtual void Plot() {
      // The order that the various graphics elements are rendered is
      // important.
      SetupLimits();
      // First plot elements that are below axis
      for (std::vector<GraphElement *>::iterator it = gelements.begin();
	   it != gelements.end(); ++it) {
	if ((*it)->drawingorder == DrawingOrder::BELOWAXIS) {
	  (*it)->Draw(this);
	}
      }
      // SVGGraph::Plot();
      PlotAxis();
      // Plot bottom elements
      for (std::vector<GraphElement *>::iterator it = gelements.begin();
	   it != gelements.end(); ++it) {
	if ((*it)->drawingorder == DrawingOrder::BOTTOM) {
	  (*it)->Draw(this);
	}
      }
    // Plot none elements
      for (std::vector<GraphElement *>::iterator it = gelements.begin();
	 it != gelements.end(); ++it) {
      if ((*it)->drawingorder == DrawingOrder::NONE) {
	(*it)->Draw(this);
      }
      }
      // Plot top elements
      for (std::vector<GraphElement *>::iterator it = gelements.begin();
	   it != gelements.end(); ++it) {
	if ((*it)->drawingorder == DrawingOrder::TOP) {
	  (*it)->Draw(this);
	}
      }
    }
    void addGraphElement(GraphElement *pe) {
      gelements.push_back(pe);
    }
    
  protected:
    void CalculateMinMax() {
      if (gelements.size() == 0) {
	return;
      }
      double tmin, tmax;
      bool isfirst = true;
      if (xlimauto) {
	for (std::vector<GraphElement *>::iterator it = gelements.begin();
	     it != gelements.end(); ++it) {
	  if (!(*it)->ongraph) {
	    continue;
	  }
	  tmin = (*it)->XMin();
	  tmax = (*it)->XMax();
	  if (isfirst) {
	    xmin = tmin;
	    xmax = tmax;
	    isfirst = false;
	  }
	  else {
	    if (tmin < xmin) {
	      xmin = tmin;
	    }
	    if (tmax > xmax) {
	      xmax = tmax;
	    }
	  }
	}
	if (xmin == xmax) {
	  xmin = xmin - 1;
	  xmax = xmax + 1;
	} 
      }
      isfirst = true;
      if (ylimauto) {
	for (std::vector<GraphElement *>::iterator it = gelements.begin();
	     it != gelements.end(); ++it) {
	  if (!(*it)->ongraph) {
	    continue;
	  }
	  tmin = (*it)->YMin();
	  tmax = (*it)->YMax();
	  if (isfirst) {
	    ymin = tmin;
	    ymax = tmax;
	    isfirst = false;
	  }
	  else {
	    if (tmin < ymin) {
	      ymin = tmin;
	    }
	    if (tmax > ymax) {
	      ymax = tmax;
	    }
	  }
	}
	if (ymin == ymax) {
	  ymin = ymin - 1;
	  ymax = ymax + 1;
	}
      }
      
    }
    
  private:
    std::vector<GraphElement *> gelements;
    
  };

  class TextElement : public GraphElement {
  public:
    TextElement() {
      xp=0.0;
      yp=0.0;
      color="black";
      font="Arial";
      fontsize = 10.0;
      textanchor = "start";
      ongraph = false;
      xshift = 0.0;
      yshift = 0.0;
    }
    void Draw(SVGBasicGraph *pgraph) {
      xmlgen::XTag *pxt;
      double nxp = xp;
      double nyp = yp;
      pgraph->ConvertPnts(nxp,nyp);
      pxt = pgraph->addText(nxp+xshift,nyp+yshift,text);
      std::string tstyle = "stroke:none;fill:" + color + ";";
      tstyle += "font-family:" + font + ";font-size:" +
	std::to_string(fontsize) + ";text-anchor:" + textanchor + ";";
      pxt->addAttribute("style",tstyle);
      
    }
    double XMin() {return xp;}
    double XMax() {return xp;}
    double YMin() {return yp;}
    double YMax() {return yp;}
    std::string text;
    double xp;
    double yp;
    std::string color;
    double fontsize;
    std::string font;
    std::string textanchor;
    double xshift;
    double yshift;
  };
  
  class PolyBaseElement : public GraphElement {
  public:
    PolyBaseElement() {;}
    virtual void Draw(SVGBasicGraph *pgraph) {;}
    double XMin() {
      if (xdata.size() == 0) {
	return 0.0;
      }
      double xm = xdata[0];
      for (std::vector<double>::iterator it=xdata.begin();
	   it != xdata.end(); ++it) {
	if (*it < xm) {
	  xm = *it;
	}
      }
      return xm;
    }
    double XMax() {
      if (xdata.size() == 0) {
	return 0.0;
      }
      double xm = xdata[0];
      for (std::vector<double>::iterator it=xdata.begin();
	   it != xdata.end(); ++it) {
	if (*it > xm) {
	  xm = *it;
	}
      }
      return xm;
    }
    double YMin() {
      if (ydata.size() == 0) {
	return 0.0;
      }
      double xm = ydata[0];
      for (std::vector<double>::iterator it=ydata.begin();
	   it != ydata.end(); ++it) {
	if (*it < xm) {
	  xm = *it;
	}
      }
      return xm;
    }
    double YMax() {
      if (ydata.size() == 0) {
	return 0.0;
      }
      double xm = ydata[0];
      for (std::vector<double>::iterator it=ydata.begin();
	   it != ydata.end(); ++it) {
	if (*it > xm) {
	  xm = *it;
	}
      }
      return xm;
    }
    
    std::vector<double> xdata;
    std::vector<double> ydata;
    
  };
  
  class LineElement : public PolyBaseElement {
  public:
    LineElement() {
      color = "blue";
      linecolor = "";
      markercolor = "";
      markerfillcolor = "";
      marker = "none";
      linestyle = "solid";
      linewidth = 1.0;
      markersize = 7.0;
      markerlinewidth = -1.0;
      markerfill = false;
    }
    
    void Draw(SVGBasicGraph *pgraph) {
      xmlgen::XTag *pxt;
      std::vector<double> newx = xdata;
      std::vector<double> newy = ydata;
      pgraph->ConvertPnts(newx,newy);
      if (linestyle.compare("none") != 0) {
	std::string lstyle = "fill:none;stroke-width:" + std::to_string(linewidth) + ";stroke:";
	if (linecolor.size()) {
	  lstyle += linecolor + ";";
	}
	else {
	  lstyle += color + ";";
	}
	pxt = pgraph->addPolyLine(newx,newy);
	pxt->addAttribute("style",lstyle);
	if (linestyle.compare("dot") == 0) {
	  pxt->addAttribute("stroke-dasharray",std::to_string(linewidth) +
			    "," + std::to_string(linewidth+1));

	}
	else if (linestyle.compare("dash") == 0) {
	  pxt->addAttribute("stroke-dasharray",std::to_string(3*linewidth) +
			    "," + std::to_string(linewidth+1));

	}
	else if (linestyle.compare("solid") != 0) {
	  pxt->addAttribute("stroke-dasharray",linestyle);
	}
      }
      if (marker.compare("none") != 0) {
	double mlw = linewidth;
	if (markerlinewidth > 0.0) {
	  mlw = markerlinewidth;
	}
	std::string mstyle = "stroke-width:" + std::to_string(mlw) + ";";
	std::string mcolortmp = color;
	if (markercolor.size()) {
	  mcolortmp = markercolor;
	}
	mstyle += "stroke:" + mcolortmp + ";";
	if (markerfill) {
	  if (markerfillcolor.size()) {
	    mcolortmp = markerfillcolor;
	  }
	  mstyle += "fill:" + mcolortmp + ";";
	}
	else {
	  mstyle += "fill:none;";
	}
	// cerr << marker << endl;
	for (uint ii = 0; ii < newx.size(); ++ii) {
	  if (marker.compare("o") == 0) {
	    pxt = pgraph->addCircle(newx[ii],newy[ii],markersize/2);
	    pxt->addAttribute("style",mstyle);
	  }
	  else if (marker.compare("x") == 0) {
	    pxt = pgraph->addLine(newx[ii]-markersize/2,newy[ii]-markersize/2,
				  newx[ii]+markersize/2,newy[ii]+markersize/2);
	    pxt->addAttribute("style",mstyle);
	    pxt = pgraph->addLine(newx[ii]-markersize/2,newy[ii]+markersize/2,
				  newx[ii]+markersize/2,newy[ii]-markersize/2);
	    pxt->addAttribute("style",mstyle);
	  }
	  else if (marker.compare("+") == 0) {
	    pxt = pgraph->addLine(newx[ii]-markersize/2,newy[ii],
				  newx[ii]+markersize/2,newy[ii]);
	    pxt->addAttribute("style",mstyle);
	    pxt = pgraph->addLine(newx[ii],newy[ii]+markersize/2,
				  newx[ii],newy[ii]-markersize/2);
	    pxt->addAttribute("style",mstyle);
	  }
	  else if (marker.compare("*") == 0) {
	    pxt = pgraph->addLine(newx[ii]-1.41421*markersize/4,newy[ii]-1.41421*markersize/4,
				  newx[ii]+1.41421*markersize/4,newy[ii]+1.41421*markersize/4);
	    pxt->addAttribute("style",mstyle);
	    pxt = pgraph->addLine(newx[ii]-1.41421*markersize/4,newy[ii]+1.41421*markersize/4,
				  newx[ii]+1.41421*markersize/4,newy[ii]-1.41421*markersize/4);
	    pxt->addAttribute("style",mstyle);
	    pxt = pgraph->addLine(newx[ii]-markersize/2,newy[ii],
				  newx[ii]+markersize/2,newy[ii]);
	    pxt->addAttribute("style",mstyle);
	    pxt = pgraph->addLine(newx[ii],newy[ii]+markersize/2,
				  newx[ii],newy[ii]-markersize/2);
	    pxt->addAttribute("style",mstyle);
	  }
	}
      }
    }
    
    std::string color;
    std::string linecolor;
    std::string markercolor;
    std::string markerfillcolor;
    std::string marker;
    std::string linestyle;
    double markersize;
    double linewidth;
    double markerlinewidth;
    bool markerfill;
  };

  class PolygonElement : public PolyBaseElement {
  public:
    PolygonElement() {
      color = "blue";
      linecolor = "";
      linestyle = "solid";
      linewidth = 1.0;
      fillcolor = "";
    }
    
    void Draw(SVGBasicGraph *pgraph) {
      xmlgen::XTag *pxt;
      std::vector<double> newx = xdata;
      std::vector<double> newy = ydata;
      pgraph->ConvertPnts(newx,newy);
      std::string lstyle = "stroke-width:" + std::to_string(linewidth) + ";stroke:";
      if (linestyle.compare("none") == 0) {
	lstyle += "none;";
      }
      else if (linecolor.size()) {
	lstyle += linecolor + ";";
      }
      else {
	lstyle += color + ";";
      }
      if (fillcolor.size()) {
	lstyle += "fill:" + fillcolor + ";";
      }
      else {
	lstyle += "fill:" + color + ";";
      }
      
      pxt = pgraph->addPolygon(newx,newy);
      pxt->addAttribute("style",lstyle);
      
    }
    
    std::string color;
    std::string linecolor;
    std::string linestyle;
    double linewidth;
    std::string fillcolor;
  };

}

#endif
