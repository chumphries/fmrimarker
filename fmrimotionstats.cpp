/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file fmrimotionstats.cpp
  \author Colin Humphries
  \brief Fmrimotionstats main function.
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "argparser.h"
#include "mrivol.h"
#include "niftifile.h"
#include "mreport.h"
#include "mrirotate.h"
#include "version.h"


enum class MOutputType {SUMMARY,FD,FDCENSOR,FDRETAIN,MFD,MXFD};

/*!
  \fn print_help
  \brief Print help message.

  This function prints a help message to stdout.
 */
void print_help(const std::string &pkg, bool isshort) {
  using namespace std;
  using namespace argparser;
  
  HelpText ht;
  ostringstream ss(ios::ate);
  ht.SetFunctionName("fmrimstats");
  ht.SetFunctionDescription("FMRI Motion Statistics");
  ht.SetFunctionVersion(MARKER_VERSION_STRING);
  // ht.AddArgument("rot","Rotation matrices file",1);
  ht.AddArgument("par","Rotation parameter file(s)",-1);
  ht.AddDescription("-par <filename(s)...>","One or more files containing a list of the six rotation parameters derived during motion correction.");

  ss.str("Rotation software package (afni,fsl) (default = ");
  ss << pkg << ")";
  ht.AddArgument("pkg",ss.str(),1);
  ht.AddDescription("-pkg <packagename>","The software used to create the rotation parameter files. (fsl,afni)");
  ht.AddArgument("out","Output results to file",1);
  ht.AddArgument("fd","Framewise displacement",0);
  ht.AddArgument("fd_mean","Mean framewise displacement",0);
  ht.AddArgument("fd_max","Max framewise displacement",0);
  ht.AddArgument("fd_censor","List of volumes with framewise displacement at or above specified threshold",1);
  ht.AddArgument("fd_retain","List of volumes with framewise displacement below specified threshold",1);
  ht.AddArgument("help","Print a detailed help message.",0);
  if (isshort) {
    cout << ht.ShortText() << endl;
  }
  else {
    cout << ht.LongText() << endl;
  }


}

/*!
  \fn main
  \brief Main function

  This is the main function for fmrimotionstats
 */
int main (int argc, const char **argv)
{
  using namespace std;
  using namespace argparser;
  
  // bool verbose = true;
  // bool sout = false;
  string matpkg = "afni";
  MOutputType mot = MOutputType::SUMMARY;
  double fdthresh = 0.9;
  
  // Parse command line options
  if (argc < 2) {
    print_help(matpkg,true);
    return 0;
  }
  ArgParser parg(argv,argc);
  if (parg.Exists("help") || parg.Exists("h")) {
    print_help(matpkg,false);
    return 0;
  }
  
  vector<string> parfilelist;
  parg.Get("par",parfilelist);
  if (!parg.Found()) {
    cerr << "Error: No rotation parameter file specified" << endl;
    return 1;
  }

  if (parg.Exists("fd")) {
    mot = MOutputType::FD;
  }
  if (parg.Exists("fd_censor")) {
    mot = MOutputType::FDCENSOR;
    parg.Get("fd_censor",fdthresh);
  }
  if (parg.Exists("fd_retain")) {
    mot = MOutputType::FDRETAIN;
    parg.Get("fd_retain",fdthresh);    
  }
  if (parg.Exists("fd_mean")) {
    mot = MOutputType::MFD;
  }
  if (parg.Exists("fd_max")) {
    mot = MOutputType::MXFD;
  }

  /*
  if (parg.Exists("foc") || parg.Exists("fd") || parg.Exists("meanfd") || parg.Exists("maxfd")) {
    verbose = false;
    sout = true;
  }
  */

  parg.Get("pkg",matpkg);

  MRIRotate mrot;
  vector<RParameters> rp;
  stringstream ss;
  for (vector<string>::iterator it=parfilelist.begin(); it != parfilelist.end(); ++it) {
    rp.clear();
    if (matpkg.compare("fsl") == 0) {
      mrot.ReadPARFSL(*it,rp);
    }
    else {
      mrot.ReadPAR(*it,rp);
    }
    if (rp.size() == 0) {
      cerr << "Error reading file " << *it << endl;
      return 1;
    }

    if (mot == MOutputType::SUMMARY) {
      if (it != parfilelist.begin()) {
	ss << "-----------------------------------" << endl;
      }
      ss << "File: " << *it << endl;
      ss << "Timepoints: " << rp.size() << endl;
      ss << "FOC: " << mrot.FOC(rp) << endl;
      double fdmean, fdmax;
      mrot.FD(rp,fdmean,fdmax);
      ss << "Mean FD: " << fdmean << endl;
      ss << "Max FD: " << fdmax << endl;
      break;
    }
    else if (mot == MOutputType::FD) {
      vector<double> fd;
      mrot.FD(rp,fd);
      for (vector<double>::iterator itd=fd.begin(); itd != fd.end(); ++itd) {
	if (itd != fd.begin()) {
	  ss << " ";
	}
	ss << *itd;
      }
      ss << endl;
      break;
    }
    else if (mot == MOutputType::FDCENSOR) {
      vector<double> fd;
      mrot.FD(rp,fd);
      bool fnm = false;
      for (uint ii=0; ii<fd.size(); ++ii) {
	if (fd[ii] >= fdthresh) {
	  if (fnm) {
	    ss << " ";
	  }
	  ss << ii;
	  fnm = true;
	}
      }
      ss << endl;
      break;
    }
    else if (mot == MOutputType::FDRETAIN) {
      vector<double> fd;
      mrot.FD(rp,fd);
      bool fnm = false;
      for (uint ii=0; ii<fd.size(); ++ii) {
	if (fd[ii] < fdthresh) {
	  if (fnm) {
	    ss << " ";
	  }
	  ss << ii;
	  fnm = true;
	}
      }
      ss << endl;
      break;
    }
    else if (mot == MOutputType::MFD || mot == MOutputType::MXFD) {
      double fdmean, fdmax;
      mrot.FD(rp,fdmean,fdmax);
      if (mot == MOutputType::MFD) {
	ss << fdmean;
      }
      else {
	ss << fdmax;
      }
      ss << endl;
    }

  }
  string fname;
  parg.Get("out",fname);
  if (parg.Found()) {
    fstream fout;
    fout.open(fname,fstream::out);
    fout << ss.str();
    fout.close();
  }
  else {
    cout << ss.str();
  }
    


  return 0;
}
