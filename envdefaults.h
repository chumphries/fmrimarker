/*********************************************************************
* Copyright 2016, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file envdefaults.h
  \author Colin Humphries
  \brief Class for loading default settings.
*/
#ifndef _ENVDEFAULTS_H
#define _ENVDEFAULTS_H

#include <string>
#include <map>
#include <stdexcept>
#include <cstdlib>
#include <algorithm>

// using namespace std;

/*! \class EnvDefaults
  \brief Class for loading parameters set in environmental variables or config files

  \author Colin Humphries
*/
class EnvDefaults {
public:
  /*!
    Class constructor.
  */
  EnvDefaults() {
    VarReset();
  }
  void AddVariable(const std::string &nm) {
    variables[nm] = "";
  }
  void AddVariable(const std::string &nm, const std::string &de) {
    variables[nm] = de;
  }
  void CheckSystem() {
    // Check environmental variables
    // todo: add configuration file checks
    char *pstr;
    for (std::map<std::string,std::string>::iterator it=variables.begin();
	 it != variables.end(); ++it) {
      pstr = std::getenv(it->first.c_str());
      if (pstr != NULL) {
	it->second = pstr;
      }
    }
  }
  void SetPackageName(const std::string &pn) {packagename = pn;}
  void Pull(const std::string &ta, std::string &sout) {
    VarReset();
    std::map<std::string,std::string>::iterator it;
    it = variables.find(ta);
    if (it != variables.end()) {
      if (!(it->second).empty()) {
	sout = it->second;
	wasfound = true;
      }
    }
  }
  void PullLower(const std::string &ta, std::string &sout) {
    VarReset();
    std::map<std::string,std::string>::iterator it;
    it = variables.find(ta);
    if (it != variables.end()) {
      sout = it->second;
      if (!(it->second).empty()) {
	sout = it->second;
	transform(sout.begin(),sout.end(),sout.begin(), ::tolower);
	wasfound = true;
      }
    }
  }
  void PullInt(const std::string &ta, int &ival) {
    VarReset();
    std::map<std::string,std::string>::iterator it;
    it = variables.find(ta);
    if (it != variables.end()) {
      if (!(it->second).empty()) {
	wasfound = true;
	int tval;
	try {
	  tval = std::stol(it->second);
	}
	catch (std::exception &e) {
	  iserror = true;
	  return;
	}
	ival = tval;
      }
    }
  }
  void PullUInt(const std::string &ta, uint &ival) {
    int tval;
    PullInt(ta,tval);
    if (iserror) {
      return;
    }
    if (tval < 0) {
      iserror = true;
      return;
    }
    ival = (uint)tval;
  }
  void PullDouble(const std::string &ta, double &dval) {
    VarReset();
    std::map<std::string,std::string>::iterator it;
    it = variables.find(ta);
    if (it != variables.end()) {
      if (!(it->second).empty()) {
	wasfound = true;
	double tval;
	try {
	  tval = std::stod(it->second);
	}
	catch (std::exception &e) {
	  iserror = true;
	  return;
	}
	dval = tval;
      }      
    }
  }
  void PullBool(const std::string &ta, bool &bval) {
    VarReset();
    std::map<std::string,std::string>::iterator it;
    it = variables.find(ta);
    if (it != variables.end()) {
      std::string sval = it->second;
      transform(sval.begin(),sval.end(),
		sval.begin(), ::tolower);
      if (!sval.empty()) {
	wasfound = true;
	if (sval.compare("yes") == 0) {
	  bval = true;
	}
	else if (sval.compare("no") == 0) {
	  bval = false;
	}
	else if (sval.compare("1") == 0) {
	  bval = true;
	}
	else if (sval.compare("0") == 0) {
	  bval = false;
	}
	else if (sval.compare("on") == 0) {
	  bval = true;
	}
	else if (sval.compare("off") == 0) {
	  bval = false;
	}
	else if (sval.compare("true") == 0) {
	  bval = true;
	}
	else if (sval.compare("false") == 0) {
	  bval = false;
	}
	else {
	  iserror = true;
	}
      }
    }
  }
  bool Error() {return iserror;}
  bool Found() {return wasfound;}

private:
  void VarReset() {
    iserror = false;
    wasfound = false;
  }
  std::string packagename;
  std::map<std::string,std::string> variables;
  bool iserror;
  bool wasfound;
};


#endif
