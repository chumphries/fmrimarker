/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file fmrimarker.cpp
  \author Colin Humphries
  \brief Fmrimarker main function.
*/
/*!
  \mainpage FMRI Motion Artifact Removal using KErnel Regression

  \section intro_sec Introduction

  This is an overview.

 */

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include "argparser.h"
#include "mrivol.h"
#include "niftifile.h"
#include "marker.h"
#include "envdefaults.h"
#include "mrirotate.h"
#include "mreport.h"
#include "version.h"
#include "helptext.h"

enum class IgnType {SKIP,ZERO,TRIM};

/*!
  \fn print_help
  \brief Print help message.

  This function prints a help message to stdout.
 */
void print_help(double swidth, double twidth, int sorder,
		int torder, bool isaligned,
		bool verbose, int ignore, IgnType ity,
		const std::string &pkg, bool isshort) {
  using namespace std;
  using namespace argparser;
  HelpText ht;
  ostringstream ss(ios::ate);
  ht.SetFunctionName("fmrimarker");
  ht.SetFunctionDescription("FMRI Motion Artifact Reduction using KErnel Regression");
  ht.SetFunctionVersion(MARKER_VERSION_STRING);

  ht.AddArgument("in","Input dataset",1);
  ht.AddDescription("-in <filename>","The input is an fMRI time series in NIFTI format. FMRIMarker can be used either on the original data before motion correction or the aligned data after motion correction has been applied, which can be specified using the -aligned option.");

  ht.AddArgument("out","Corrected dataset",1);
  ht.AddDescription("-out <filename>","The output is a version of the input file that has been cleaned of motion artifact.");

  ht.AddArgument("mat","Rotation matrix file(s)",-1);
  ht.AddDescription("-mat <filename(s)...>",FM_HELPTEXT_MATOPT);
  
  ss.str("Rotation software package (afni,fsl) (default = ");
  ss << pkg << ")";
  ht.AddArgument("pkg",ss.str(),1);
  ht.AddDescription("-pkg <packagename>","The software used to create the rotation matrices, which defines the coordinate system that was used. Valid options include: afni, fsl, nifti. The nifti option is used to specify a generic NIFTI coordinate system.");

  ht.AddArgument("fsldir","Load FSL rotation matrices (i.e., MAT_0000 MAT_0001 ...) from specified directory",1);
  ht.AddDescription("-fsldir <directoryname>","A directory created by mcflirt that contains a series of 4x4 rotation matrices with the naming scheme: MAT_0000, MAT_OOO1, ...\n* When this option is used it will also enable the '-pkg fsl' option by default.");
  vector<string> atmp;
  atmp.push_back("-fslref <filename>");
  atmp.push_back("-fslorig <filename>");
  ht.AddDescription(atmp,"FSL rotation matrices cannot be interpreted without knowing the orientation and image dimensions of the files that were used in the alignment. By default, it is assumed that the orientation of the original data and reference files used in mcflirt have the same orientation as the input file to FMRIMarker. If, however, a reference file was used in mcflirt with different orientation or image dimensions than the original data file, then the additional file must be specified using either the -fslref or -fslorig options.\n* If the input to FMRIMarker is the original data before motion correction (i.e., using '-aligned no') then -fslref should be specified with the name of the reference file.\n* If the input to FMRIMarker is the spatially realigned data after motion correction (i.e., '-aligned yes') then -fslorig should be specified with the name of the original data file.\n* Note that if using the '-aligned yes' option and several spatial transformations have been applied, make sure to use the concatonated version of the matrices.");

  ht.AddArgument("fslref","Reference volume used by fsl/mcflirt. (If reference and original had different orientations/sizes and -aligned no)",1);
  ht.AddArgument("fslorig","Original fMRI input to fsl/mcflirt. (If reference and original had different orientations/sizes and -aligned yes)",1);
  
  
  // ht.AddArgument("mask","Mask volume",1);
  ss.str("Kernel FWHM <spatial> <temporal> (default = ");
  ss << swidth << " " << twidth << ")";
  ht.AddArgument("fwhm",ss.str(),2);

  ss.str("Kernel spatial FWHM (mm) (default = ");
  ss << swidth << ")";  
  ht.AddArgument("s_fwhm",ss.str(),1);

  ss.str("Kernel temporal FWHM (sec) (default = ");
  ss << twidth << ")";  
  ht.AddArgument("t_fwhm",ss.str(),1);
  atmp.clear();
  atmp.push_back("-fwhm <spatial> <temporal>");
  atmp.push_back("-s_fwhm <spatial>");
  atmp.push_back("-t_fwhm <temporal>");
  ht.AddDescription(atmp,"The widths of the kernel used in kernel regression.\n* Note that using kernel widths that are too small can result in overfitting. The default values should work for the majority of datasets; however, best practice would be to run FMRIMarker on your data using varying kernel widths to confirm that overfitting is not occurring.\n* Note that it is best to use relatively large values for width of the temporal kernel. If the temporal kernel is too narrow then the analysis will begin to fit the low frequency BOLD fluctuations.");

  ht.AddArgument("mean","Output initial voxel means",1);
  ht.AddDescription("-mean <filename>","After using kernel regression, the time course of each voxel in the output volume is mean zero. This option saves the original mean of each voxel in a file. This might be useful for later processing, such as calculating percent signal change.");

  ht.AddArgument("fit","Output estimated motion time courses",1);
  ht.AddDescription("-fit <filename>","Output an fMRI file with the estimated motion time courses of each voxel.\n* Note that the fitted time courses could also be calculated by subtracting the output file from the input file.");

  ht.AddArgument("stats","Output model R-squared, degrees of freedom, sum-of-squares",1);
  ht.AddDescription("-stats <filename>","This option saves an MRI file with three subvolumes.\n* The first volume contains the R-Squared values of the model, which are calculated by\n        sum((y(i) - ym)*(yhat(i) - ym)) ^ 2\n  R^2 = -------------------------------------\n        sum((y(i)-ym)^2) * sum((yhat(i)-ym)^2)\nwhere\n  y(i) is the original time course of the voxel at each time point i\n  yhat(i) is the estimated time course of motion in the voxel at each time point i\n  ym is the mean of y(i)\n* The second and third volumes can be used for estimating the degrees of freedom of the kernel regression model. Note that the degrees of freedom of the model vary depending on the amount of movement during the run, which will vary across voxels. If W is the weight matrix used in the kernel regression (i.e., yhat = W * y), then the second volume contains the sum of the diagonals of W (i.e., trace(W)) and the third volume is trace(W'*W). The degrees of freedom of the residuals is 2*trace(W)-trace(W'*W).\n* The fourth volume is the sum-of-squares of the orginal data.\n* The fifth volume is the sum-of-squares of the residuals (i.e., cleaned data).\n* The sixth volume is the sum-of-squares of the model (ie., estimated motion time course).");
  ht.AddArgument("dvars","Output text file with DVars before/after/difference",1);
  ss.str("Number of initial volumes to ignore (default = ");
  ss << ignore << ")";
  ht.AddArgument("ignore",ss.str(),1);  
  // ht.AddArgument("skip","Leave ignored volumes unchanged in the output dataset",0);  
  if (ity == IgnType::SKIP) {
    ht.AddArgument("skip","Leave ignored volumes unchanged in the output dataset (default)",0);  
  }
  else {
    ht.AddArgument("skip","Leave ignored volumes unchanged in the output dataset",0);  
  }
  if (ity == IgnType::ZERO) {
    ht.AddArgument("zero","Set ignored volumes to zero in the output (default)",0);
  }
  else {
    ht.AddArgument("zero","Set ignored volumes to zero in the output",0);
  }
  if (ity == IgnType::TRIM) {
    ht.AddArgument("trim","Remove ignored volumes in the output (default)",0);
  }
  else {
    ht.AddArgument("trim","Remove ignored volumes in the output",0);
  }
  atmp.clear();
  atmp.push_back("-ignore <num>");
  atmp.push_back("-skip");
  atmp.push_back("-zero");
  atmp.push_back("-trim");
  ht.AddDescription(atmp,"The signal in fMRI scans often shows a large drop in the first few images, so it is best to remove these volumes from the MARKER analysis.\n* Use -ignore to specify how many volumes are ignored. (e.g., 3)\n* Using -skip will keep the ignored volumes unchanged in the output dataset.\n* Using -zero will set the ignored volumes to zero in the output dataset.\n* Using -trim will completely remove the ignored volumes, producing an output dataset with fewer volumes than the original input.");

  // ss.str("Use N'th order polynomial terms <spatial> <temporal> (default = ");
  // ss << sorder << " " << torder << ")";
  // ht.AddArgument("order",ss.str(),2);
  // ss.str("Use N'th order polynomial terms along spatial dimensions (default = ");
  // ss << sorder << ")";
  // ht.AddArgument("s_order",ss.str(),1);
  // ss.str("Use N'th order polynomial term along temporal dimension (default = ");
  // ss << torder << ")";
  // ht.AddArgument("t_order",ss.str(),1);

if (isaligned) {
    ht.AddArgument("aligned","Input dataset has been processed and resampled by a motion correction algorithm (yes/no) (default = yes)",0);
  }
  else {
    ht.AddArgument("aligned","Input dataset has been processed and resampled by a motion correction algorithm (yes/no) (default = no)",0);
  }
  ht.AddDescription("-aligned <yes/no>","This option specifies whether the input data have been resampled using a motion correction algorithm. This primarily affects how the voxel positions are calculated using the rotation matrices.");
  /*
  if (isorig) {
    ht.AddArgument("aligned","Input dataset has been processed and resampled by a motion correction algorithm",0);
    ht.AddArgument("orig","Input dataset has not been motion corrected (default)",0);  
  }
  else {
    ht.AddArgument("aligned","Input dataset has been processed and resampled by a motion correction algorithm (default)",0);
    ht.AddArgument("orig","Input dataset has not been motion corrected",0);  
  }
  atmp.clear();
  atmp.push_back("-aligned");
  atmp.push_back("-orig");
  ht.AddDescription(atmp,"These options specify whether the input data have been resampled using a motion correction algorithm. This primarily affects how the voxel positions are calculated using the rotation matrices.");
  */
  
  ht.AddArgument("tr","Use a different TR (sec) than what is listed in dataset header",1);
  ht.AddDescription("-tr <num>","The TR should correspond to the acquisition time of each volume. By default, FMRIMarker loads this value from the dataset header. Use this option to specify a different value.");

  ht.AddArgument("double","Save data using 64-bit (instead of 32-bit) floating point numbers",0);
  ht.AddDescription("-double","All internal calculations in FMRIMarker are performed using 64-bit floating point numbers. By default, the program will save the output file using 32-bit floating point numbers. Use this option to save in 64-bit format.");

  ht.AddArgument("report","Generate analysis report in HTML format",1);
  ht.AddDescription("-report <filename>","Save a report in HTML format. This report can be viewed in any web-browser. It contains:\n* Information about the analysis.\n* Graphs of the 6 motion parameters of the input. (Note that the six degrees of rotation are estimated from the rotation matrices and may not be identical to the parameters that are generated by the motion correction algorithm.)\n* Graph of Framewise Displacement.\n* Graph of DVars before and after artifact removal.");

  ht.AddArgument("experiment","Experiment ID",1);
  ht.AddArgument("subject","Subject ID",1);
  ht.AddArgument("session","Session ID",1);
  ht.AddArgument("run","Run ID",1);
  atmp.clear();
  atmp.push_back("-experiment <string>");
  atmp.push_back("-subject <string>");
  atmp.push_back("-session <string>");
  atmp.push_back("-run <string>");
  ht.AddDescription(atmp,"These variables can be used to specify optional information about the fMRI run that will be included in the final HTML report file.");

  ht.AddArgument("par","Rotation parameter file",1);
  ht.AddDescription("-par <filename>","If this option is specified then the rotation, translation, and framewise displacement graphs in the HTML report will be generating using the specified motion parameter file instead of using estimates from the rotation matrices.\n* Note that this only affects the graphs in the report and does not change the analysis.");
 
  ss.str("Display additional information: on/off (default = ");
  if (verbose) {
    ss << "on" << ")";
  }
  else {
    ss << "off" << ")";
  }

  ht.AddArgument("verbose",ss.str(),1);
  ht.AddArgument("help","Print a detailed help message.",0);

  if (isshort) {
    cout << ht.ShortText() << endl;
  }
  else {
    cout << ht.LongText() << endl;
  }
}

/*!
  \fn main
  \brief Main function

  This is the main function for fmrimarker
 */
int main (int argc, const char **argv)
{
  using namespace std;
  using namespace argparser;
  using namespace fmri;
  
  bool verbose = false;
  // bool usedouble = false;
  double s_fwhm = 0.1, t_fwhm = 60.0, tr = -1.0;
  uint s_order = 0, t_order = 0;
  uint skip;
  string skiptype = "zero";
  string matpkg = "afni";
  IgnType ignoretype;
  int niftidatatype = NIFTI_TYPE_FLOAT32;
  NIFTIFile nin, nin2;

  bool isaligned = true;
  bool debug = false;

  // Load configuration settings /////////////////////////////////////
  EnvDefaults envd;
  envd.SetPackageName("fmrimarker");
  // Set defaults
  // envd.AddVariable("FMRIMARKER_SPATIAL_KERNEL","YES");
  // envd.AddVariable("FMRIMARKER_TEMPORAL_KERNEL","NO");
  envd.AddVariable("FMRIMARKER_SPATIAL_FWHM","0.1");
  envd.AddVariable("FMRIMARKER_TEMPORAL_FWHM","60.0");
  // envd.AddVariable("FMRIMARKER_SPATIAL_ORDER","0");
  // envd.AddVariable("FMRIMARKER_TEMPORAL_ORDER","0");
  envd.AddVariable("FMRIMARKER_IGNORE_MODE","ZERO"); // SKIP, ZERO, TRIM
  envd.AddVariable("FMRIMARKER_IGNORE_NUM","2");
  envd.AddVariable("FMRIMARKER_MAT_PACKAGE","AFNI");
  envd.AddVariable("FMRIMARKER_ALIGNED","YES");
  envd.AddVariable("FMRIMARKER_VERBOSE","NO");

  // Get values from environmental variables and config files
  envd.CheckSystem();
  
  // Retrieve and check values
  envd.PullDouble("FMRIMARKER_SPATIAL_FWHM",s_fwhm);
  if (envd.Error()) {
    string tstr;
    envd.Pull("FMRIMARKER_SPATIAL_FHWM",tstr);
    cerr << "Error: Invalid setting for environmental variable fMRIMARKER_SPATIAL_FHWM: " 
	 << tstr << endl;
    return 1;
  }
  envd.PullDouble("FMRIMARKER_TEMPORAL_FWHM",t_fwhm);
  if (envd.Error()) {
    string tstr;
    envd.Pull("FMRIMARKER_TEMPORAL_FHWM",tstr);
    cerr << "Error: Invalid setting for environmental variable FMRIMARKER_TEMPORAL_FHWM: " 
	 << tstr << endl;
    return 1;
  }
  envd.PullLower("FMRIMARKER_IGNORE_MODE",skiptype);
  if (skiptype.compare("skip") == 0) {
    ignoretype = IgnType::SKIP;
  }
  else if (skiptype.compare("zero") == 0) {
    ignoretype = IgnType::ZERO;
  }
  else if (skiptype.compare("trim") == 0) {
    ignoretype = IgnType::TRIM;
  }
  else {
    cerr << "Error: Unknown value for ignore type" << endl;
    return 1;
  }
  envd.PullUInt("FMRIMARKER_IGNORE_NUM",skip);
  if (envd.Error()) {
    string tstr;
    envd.Pull("FMRIMARKER_IGNORE_NUM",tstr);
    cerr << "Error: Invalid value for configuration setting FMRIMARKER_IGNORE_NUM: " 
	 << tstr << endl;
    return 1;
  }
  
  envd.PullBool("FMRIMARKER_VERBOSE",verbose);
  envd.PullBool("FMRIMARKER_ALIGNED",isaligned);
  envd.PullLower("FMRIMARKER_MAT_PACKAGE",matpkg);

  // Parse command line options /////////////////////////////////////
  if (argc < 2) {
    print_help(s_fwhm,t_fwhm,s_order,t_order,isaligned,verbose,
	       skip,ignoretype,matpkg,true);
    return 0;
  }
  ArgParser parg(argv,argc);
  if (parg.Exists("help") || parg.Exists("h")) {
    print_help(s_fwhm,t_fwhm,s_order,t_order,isaligned,verbose,skip,ignoretype,matpkg,false);
    return 0;
  }
  if (parg.Exists("debug")) {
    debug = true;
  }
  // Read filenames
  string infile, outfile, matfile, maskfile, fitfile, reportfile, statsfile, parfile, dvfile;
  parg.Get("in",infile);
  if (!parg.Found() || parg.Error()) {
    cerr << "Error: No input file specified" << endl;
    return 1;
  }
  /*
  parg.Get("out",outfile);
  if (!parg.Found() || parg.Error()) {
    cerr << "Error: No output file specified" << endl;
    return 1;
  }
  */

  // Read kernel parameters 
  if (parg.Exists("fwhm")) {
    parg.Get("fwhm",0,s_fwhm);
    bool terr = parg.Error();
    parg.Get("fwhm",1,t_fwhm);
    if (parg.Error() || terr) {
      cerr << "Error parsing -fwhm option" << endl;
      return 1;
    }
  }
  parg.Get("s_fwhm",s_fwhm);
  if (parg.Error()) {
    cerr << "Error parsing -s_fwhm option" << endl;
    return 1;
  }
  parg.Get("t_fwhm",t_fwhm);
  if (parg.Error()) {
    cerr << "Error parsing -t_fwhm option" << endl;
    return 1;
  }
  // Polynomial parameters
  if (parg.Exists("order")) {
    parg.Get("order",0,s_order);
    bool terr = parg.Error();
    parg.Get("order",1,t_order);
    if (parg.Error() || terr) {
      cerr << "Error parsing -order option" << endl;
      return 1;
    }
  }
  parg.Get("s_order",s_order);
  if (parg.Error()) {
    cerr << "Error parsing -s_order option" << endl;
    return 1;
  }
  parg.Get("t_order",t_fwhm);
  if (parg.Error()) {
    cerr << "Error parsing -t_order option" << endl;
    return 1;
  }
  // Initial volume skipping parameters
  parg.Get("ignore",skip);

  if (parg.Error()) {
    cerr << "Error parsing -ignore option" << endl;
    return 1;
  }
  if (parg.Exists("skip")) {
    // skiptype = "skip";
    ignoretype = IgnType::SKIP;
  }
  else if (parg.Exists("zero")) {
    // skiptype = "zero";
    ignoretype = IgnType::ZERO;
  }
  else if (parg.Exists("trim")) {
    // skiptype = "trim";
    ignoretype = IgnType::TRIM;
  }
  // TR
  parg.Get("tr",tr);
  if (parg.Error()) {
    cerr << "Error parsing -tr option" << endl;
    return 1;
  }
  // Alignment parameters
  /*
  if (parg.Exists("orig")) {
    isorig = true;
  }
  if (parg.Exists("aligned")) {
    isorig = false;
  }
  */
  parg.Get("aligned",isaligned);
  if (parg.Error()) {
    cerr << "Error parsing -aligned option" << endl;
    return 1;
  }
  // Verbose
  parg.Get("verbose",verbose);
  if (parg.Error()) {
    cerr << "Error parsing -verbose option" << endl;
    return 1;
  }
  if (parg.Exists("double")) {
    niftidatatype = NIFTI_TYPE_FLOAT64;
  }
  /*
  if (skip < 0) {
    cerr << "Number of initial volumes to be removed must be non-negative" << endl;
    return 1;
  }
  */


  // Load data files //////////////////////////////////////////////////
  MRIData<double> data;
  MRIData<double> model;
  MRIData<double> degf;
  // input fMRI time series
  if (!nin.Open(infile,'r')) {
    cerr << "Error opening input file" << endl;
    return 1;
  }
  if (nin.NumDims() < 4) {
    cerr << "Error: input data must have more than 3 dimensions" << endl;
    return 1;
  }
  // cout << skip << endl;
  if ((int)skip >= nin.Size(3)) {
    cerr << "Error: number of skipped time points is greater or equal to the total number of time points" << endl;
    return 1;
  }  
  if (verbose) {
    cout << "Input file: " << infile << endl;
    cout << "Voxels: " << nin.Size(0) << " x " << nin.Size(1) << " x " << nin.Size(2) << endl;
    cout << "Time points: " << nin.Size(3) << endl;
    if (tr <= 0.0) {
      cout << "TR: " << nin.VoxelSize(3) << endl;
    }
    else {
      cout << "User defined TR: " << tr << endl;
    }
  }
  if (tr <= 0.0) {
    tr = nin.VoxelSize(3);
  }
  if (tr <= 0.0) {
    cerr << "Error: TR is less than or equal to 0" << endl;
    return 1;
  }
  // Note: if a maskfile is specified then the data is stored as an indexed array.
  // (See MRIData object structure for details)
  // Note: using a mask only makes sense for data that has been motion corrected.
  // If the data has not been corrected then some of the voxels might have moved
  // outside of the mask.
  parg.Get("mask",maskfile);
  if (parg.Found()) {
    // NIFTIFile nin2;
    if (!nin2.Open(maskfile,'r')) {
      cerr << "Error opening mask file" << endl;
      return 1;
    }
    if ((nin2.XSize() != nin.XSize()) || 
	(nin2.YSize() != nin.YSize()) || 
	(nin2.ZSize() != nin.ZSize())) {
      cerr << "Error: mask volume must be the same size as input volume" << endl;
      return 1;
    }
    MRIVol<float> mask;
    if (!nin2.Read(mask)) {
      cerr << "Error reading mask file" << endl;
      return 1;
    }
    nin2.Close();
    if (!nin.Read(data,mask)) {
      cerr << "Error reading input data" << endl;
      return 1;
    }
    model.ResizeIndexed(mask,data.NumTimePnts(),data.TVoxelSize());
    degf.ResizeIndexed(mask,2);
  }
  else {
    if (!nin.Read(data)) {
      cerr << "Error reading input data" << endl;
      return 1;
    }
    model.Resize(data);
    degf.Resize(data,2);
  }
  nin.Close();  // Note: header info remains in object after closure. Any additional file input
                // after this point should be done with nin2 because we will use the header 
                // info from nin later.

  // Set parameters based on the software package //////////////////////////////////////
  // All rotation matrices in this code are represented with the NIFTI coordinate system.
  // The MRIRotate class will convert AFNI and FSL matrices to the correct coordinate system
  if (parg.Exists("fsldir")) {
    matpkg = "fsl";
  }
  MRIRotate mrot;
  parg.Get("pkg",matpkg);
  if (matpkg.compare("afni") == 0) {
    mrot.SetInSpace(RotationSpace::DICOM);
    mrot.ApplyInverse(true);
  }
  else if (matpkg.compare("fsl") == 0) {
    mrot.SetInSpace(RotationSpace::FSL);
    MRIBase source(data);
    MRIBase target(data);
    string afile1;
    parg.Get("fslorig",afile1);
    if (parg.Found()) {
      if (!nin2.Open(afile1,'r')) {
	cerr << "Error: opening fslorig file" << endl;
	return 1;
      }
      nin2.Read(source);
      nin2.Close();
    }
    parg.Get("fslref",afile1);
    if (parg.Found()) {
      if (!nin2.Open(afile1,'r')) {
	cerr << "Error opening fslref file" << endl;
	return 1;
      }
      nin2.Read(target);
      nin2.Close();
    }
    mrot.SetInSpaceFSL(source,target);
  }
  else if (matpkg.compare("nifti") == 0) {
    mrot.SetInSpace(RotationSpace::NIFTI);
  }
  else {
    cerr << "Error: unknown option for -pkg: " << matpkg << endl;
    return 1;
  }
  // Load rotation matrices //////////////////////////////////
  vector<OrientationMatrix> matdata;
  vector<string> matfiles;
  parg.Get("mat",matfiles);
  if (parg.Found()) {
    mrot.ReadMAT(matfiles,matdata);
    matfile = matfiles[0];
    if (!isaligned) {
      // Check this. It looks wrong.
      OrientationMatrix omat = matdata[0];
      omat.Invert();
      for (vector<OrientationMatrix>::iterator it=matdata.begin(); it != matdata.end(); ++it) {
	*it = omat * (*it);
      }
    }
  }
  else {
    parg.Get("fsldir",matfile);
    if (parg.Found()) {
      mrot.ReadFSLDir(matfile,matdata);
    }
    else {
      cerr << "Error: No rotation file/directory specified" << endl;
      return 1;
    }
  }
  if (verbose) {
    cout << "Rotation matrix file/dir: " << matfile << endl;
    cout << "Number of rotation matrices: " << matdata.size() << endl;
  }
  if (matdata.size() != data.NumTimePnts()) {
    cerr << "Error: Number of rotation matrices does not equal number of time points" << endl;
    return 1;
  }

  // Output optional mean file ////////////////////////////////
  parg.Get("mean",maskfile);
  if (parg.Found()) {
    if (verbose) {
      cout << "Saving voxel means to file: " << maskfile << endl;
    }
    // Estimate voxel means ignoring "skipped" volumes
    MRIData<double> means;
    means.Resize(data,1);
    for (size_t ii=0; ii<data.NumVoxels(); ++ii) {
      means(0,ii) = 0.0;
      for (size_t jj=skip; jj<data.NumTimePnts(); ++jj) {
	means(0,ii) += data(jj,ii);
      }
      means(0,ii) /= (double)(data.NumTimePnts()-skip);
    }
    // Save means in a file
    if (!nin2.Open(maskfile,'w')) {
      cout << "Error opening mean file" << endl;
      return 1;
    }
    if (!nin2.Write(means,niftidatatype)) {
      cout << "Error writing to mean file: " << maskfile << endl;
      return 1;
    }
    nin2.Close();
  }

  // Run MARKER analysis /////////////////////////////////////
  Marker mr;
  if (debug) {
    mr.SetDebug(true);
  }
  mr.SetSkip(skip);
  mr.SetFWHM(s_fwhm);
  // Note: the temporal fwhm for the Marker Object is in timepoints not seconds
  mr.SetTrendFWHM(t_fwhm/tr);
  mr.SetOrder(s_order);
  mr.SetTrendOrder(t_order);
  if (verbose) {
    cout << "Spatial FWHM: " << s_fwhm << endl;
    cout << "Temporal FWHM: " << t_fwhm << endl;
    cout << "Starting analysis..." << endl;
  }
  mr.SetPositions(data,matdata);
  if (!parg.Exists("nomarker")) {
    mr.Analyze(data,model,degf);
    if (verbose) {
      double maxdf, mindf,dftmp;
      maxdf = degf(0,0)*2.0 - degf(1,0);
      mindf = maxdf;
      for (size_t ii=1; ii<degf.NumVoxels(); ++ii) {
	dftmp = degf(0,ii)*2.0 - degf(1,ii);
	if (dftmp > maxdf) {
	  maxdf = dftmp;
	}
	if (dftmp < mindf) {
	  mindf = dftmp;
	}
      }
      cout << "Min degrees of freedom: " << mindf << endl;
      cout << "Max degrees of freedom: " << maxdf << endl;
      // Maybe add a warning here if deg freedom is too high?
    }
  }
  else {
    cout << "Skipping marker analysis" << endl;
  }

  // Output remaining data files /////////////////////////////
  NIFTIFile nout;
  vector<int> subindex;
  if (ignoretype == IgnType::TRIM) {
    for (size_t ii = skip; ii<data.NumTimePnts(); ++ii) {
      subindex.push_back(ii);
    }
  }
  if (verbose) {
    cout << "Saving data files" << endl;
  }
  parg.Get("fit",fitfile);
  if (parg.Found()) {
    if (verbose) {
      cout << "Saving fit file: " << fitfile << endl;
    }
    if (!nout.Open(fitfile,'w')) {
      cerr << "Error opening fit file" << endl;
      return 1;
    }
    nout.CopyHeaderOrient(nin);
    nout.CopyHeaderIntent(nin);
    nout.CopyHeaderUnits(nin);
    nout.CopyHeaderSliceInfo(nin);
    if (ignoretype == IgnType::TRIM) {
      if (!nout.Write(model,subindex,niftidatatype)) {
	cerr << "Error writing to fit file" << endl;
	return 1;
      }
    }
    else {
      if (!nout.Write(model,niftidatatype)) {
	cerr << "Error writing to fit file" << endl;
	return 1;
      }
    }
    nout.Close();
  }
  
  // Output stats file ///////////////////////////////////////
  parg.Get("stats",statsfile);
  if (parg.Found()) {
    MRIData<double> stats;
    stats.Resize(degf,6);
    stats.SetSubVolume(1,degf,0);
    stats.SetSubVolume(2,degf,1);
    mr.RSquared(data,model,stats);
    if (verbose) {
      cout << "Saving stats file: " << statsfile << endl;
    }
    if (!nout.Open(statsfile,'w')) {
      cerr << "Error opening stats file" << endl;
      return 1;
    }
    nout.CopyHeaderOrient(nin);
    if (!nout.Write(stats,niftidatatype)) {
      cerr << "Error writing to stats file" << endl;
      return 1;
    }
    nout.Close();
  }
  
  // Output cleaned data file ////////////////////////////////
  parg.Get("out",outfile);
  if (parg.Found()) {
    if (!nout.Open(outfile,'w')) {
      cerr << "Error opening output file" << endl;
      return 1;
    }
    nout.CopyHeaderOrient(nin);
    nout.CopyHeaderIntent(nin);
    nout.CopyHeaderUnits(nin);
    nout.CopyHeaderSliceInfo(nin);
    // if (skiptype.compare("trim") == 0) {
    if (ignoretype == IgnType::TRIM) {
      // mr.ResidualTrim(data,model);
      mr.ResidualZero(data,model);
      if (!nout.Write(model,subindex,niftidatatype)) {
	cerr << "Error writing to output file" << endl;
	return 1;
      }
    }
    else {
      if (skiptype.compare("zero") == 0) {
	mr.ResidualZero(data,model);
      }
      else {
	mr.ResidualSkip(data,model);
      }
      if (!nout.Write(model,niftidatatype)) {
	cerr << "Error writing to output file" << endl;
	return 1;
      }
    }
    nout.Close();
  }
  
  // Output dvars file ///////////////////////////////////////
  parg.Get("dvars",dvfile);
  if (parg.Found()) {
    fstream fout;
    fout.open(dvfile,fstream::out);
    if (fout.fail()) {
      cerr << "Error opening dvars file" << endl;
    }
    else {
      double dv1,dv2;
      for (size_t ii=skip+1; ii<data.NumTimePnts(); ++ii) {
	dv1 = 0.0;
	dv2 = 0.0;
	for (size_t jj=0; jj<data.NumVoxels(); ++jj) {
	  dv1 += (data(ii,jj)-data(ii,jj))*(data(ii,jj)-data(ii,jj));
	  dv2 += (model(ii,jj)-model(ii,jj))*(model(ii,jj)-model(ii,jj));
	}
	dv1 = sqrt(dv1/((double)data.NumVoxels()));
	dv2 = sqrt(dv2/((double)data.NumVoxels()));
	fout << dv1 << " " << dv2 << " " << (dv1-dv2) << endl;
      }
      fout.close();
    }


  }

  // Output report file //////////////////////////////////////
  parg.Get("report",reportfile);
  if (parg.Found()) {
    if (verbose) {
      cout << "Generating HTML report" << endl;
    }
    MarkerReport mrkrep;
    /*
    mrkrep.setSkip(skip);
    mrkrep.setSSTD(s_fwhm);
    mrkrep.setTSTD(t_fwhm);
    mrkrep.setInFile(infile);
    mrkrep.setOutFile(outfile);
    mrkrep.setMatFile(matfile);
    */
    mrkrep.AddSubjectInfo(parg);
    mrkrep.AddInfo(s_fwhm,t_fwhm,infile,matfile);
    parg.Get("par",parfile);
    if (parg.Found()) {
      if (verbose) {
	cout << "Using rotation parameters from file " << parfile << endl;
      }
      MRIRotate mrot2;
      vector<RParameters> rp2;
      if (matpkg.compare("fsl") == 0) {
	mrot2.ReadPARFSL(parfile,rp2);
      }
      else {
	mrot2.ReadPAR(parfile,rp2);
      }
      if (rp2.size() == 0) {
	cerr << "Error reading file " << parfile << endl;
	return 1;
      }
      mrkrep.AddMATData(rp2);
    }
    else {
      mrkrep.AddMATData(matdata,data);
    }
    mrkrep.AddTCData(data,model,skip);
    fstream fout;
    fout.open(reportfile,fstream::out);
    if (fout.fail()) {
      cerr << "Error opening report file" << endl;
    }
    else {
      fout << "<!DOCTYPE html>" << endl << mrkrep.Render() << endl;
      fout.close();
    }
  }


  return 0;
}
