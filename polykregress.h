#ifndef _POLYKREGRESS_H
#define _POLYKREGRESS_H

// void polykregress(double *X, int M, int N, double *Y, double *sigma);
void polykregress(const double *X, int M, int N, int L, 
		  const double *Y, int Q, double *Yh, double *df,
		  const double *sigma, bool matched, int skip);
#endif
