/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file fmrimreport.cpp
  \author Colin Humphries
  \brief Fmrimreport main function.
*/

#include <iostream>
#include <fstream>
#include <string>
#include "argparser.h"
#include "mrivol.h"
#include "niftifile.h"
#include "mrirotate.h"
#include "mreport.h"
#include "version.h"

/*!
  \fn print_help
  \brief Print help message.

  This function prints a help message to stdout.
 */
void print_help(const std::string &pkg, bool isshort) {
  using namespace std;
  using namespace argparser;
  
  HelpText ht;
  ostringstream ss(ios::ate);
  ht.SetFunctionName("fmrimreport");
  ht.SetFunctionDescription("FMRI Motion Report");
  ht.SetFunctionVersion(MARKER_VERSION_STRING);

  ht.AddArgument("par","Rotation parameter file(s)",-1);
  ht.AddDescription("-par <filename(s)...>","One or more files containing a list of the six rotation parameters derived during motion correction.");

  ht.AddArgument("out","Output file (HTML format)",1);
  ht.AddDescription("-out <filename>","The output is an HTML document with information and graphs of the amount of movement during the fMRI run(s).");

  ss.str("Rotation software package (afni,fsl) (default = ");
  ss << pkg << ")";
  ht.AddArgument("pkg",ss.str(),1);
  ht.AddDescription("-pkg <packagename>","The software used to create the rotation parameter files. (fsl,afni)");
  // ht.AddArgument("subject","Subject ID",1);
  // ht.AddArgument("study","Study ID",1);
  // ht.AddArgument("session","Session ID",1);

  // ht.AddArgument("rtype","Rotation matrices file type (afni,fsl,spm,ants)",1);
  // ht.AddArgument("mask","Mask volume (optional)",1);
  // ht.AddArgument("base","Base volume if different from input (optional)",1);

  ht.AddArgument("experiment","Experiment ID",1);
  ht.AddArgument("subject","Subject ID",1);
  ht.AddArgument("session","Session ID",1);
  ht.AddArgument("run","Run ID",1);
  vector<string> atmp;
  atmp.push_back("-experiment <string>");
  atmp.push_back("-subject <string>");
  atmp.push_back("-session <string>");
  atmp.push_back("-run <string>");
  ht.AddDescription(atmp,"These variables can be used to specify optional information about the fMRI run that will be included in the report file.");

  ht.AddArgument("help","Print a detailed help message.",0);
  if (isshort) {
    cout << ht.ShortText() << endl;
  }
  else {
    cout << ht.LongText() << endl;
  }

}

/*!
  \fn main
  \brief Main function

  This is the main function for fmrimreport
 */
int main (int argc, const char **argv)
{
  using namespace std;
  using namespace argparser;

  string matpkg = "afni";

  // Parse command line options /////////////////////////////////////
  if (argc < 2) {
    print_help(matpkg,true);
    return 0;
  }
  ArgParser parg(argv,argc);
  if (parg.Exists("help") || parg.Exists("h")) {
    print_help(matpkg,false);
    return 0;
  }
  string outfile;
  vector<string> parfilelist;
  parg.Get("par",parfilelist);
  if (!parg.Found()) {
    cerr << "Error: No rotation parameter file specified" << endl;
    return 1;
  }

  parg.Get("out",outfile);
  if (!parg.Found() || parg.Error()) {
    cerr << "Error: output file not specified" << endl;
    return 1;
  }
  parg.Get("pkg",matpkg);

  MotionReport mrep;
  mrep.AddSubjectInfo(parg);
  MRIRotate mrot;
  vector<RParameters> rp;
  
  for (vector<string>::iterator it=parfilelist.begin(); 
       it != parfilelist.end(); ++it) {
    rp.clear();
    if (matpkg.compare("fsl") == 0) {
      mrot.ReadPARFSL(*it,rp);
    }
    else {
      mrot.ReadPAR(*it,rp);
    }
    if (rp.size() == 0) {
      cerr << "Error reading file " << *it << endl;
      return 1;
    }
    mrep.AddRun(rp,*it);

  }
  fstream fout;
  fout.open(outfile,fstream::out);
  if (fout.fail()) {
    cerr << "Error opening output file" << endl;
    return 1;
  }
  fout << "<!DOCTYPE html>" << endl << mrep.Render() << endl;
  fout.close();
  return 0;
}
