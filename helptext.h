/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file helptext.h
  \author Colin Humphries
  \brief Common text definitions for help messages
 */
#ifndef _HELPTEXT_H
#define _HELPTEXT_H

#define FM_HELPTEXT_MATOPT "A single text file or list of files containing a series of rotation matrices. If several files are used then each file should contain a 4 x 4 rotation matrix with the following arrangement:\n  A B C D\n  E F G H\n  I J K L\n  0 0 0 1\nIf a single file is used then it can either be formatted as an N x 16 matrix with each row arranged as follows:\n  A B C D E F G H I J K L 0 0 0 1\nor an N x 12 matrix:\n  A B C D E F G H I J K L\n* Note that the N x 12 format corresponds to the AFNI .aff12.1D format.\n* FSL will generally create a series of 4 x 4 matrix files. It is also possible to instead specify the directory that the FSL rotation matrices are stored using the -fsldir option.\n* Internally, FMRIMarker represents all rotation matrices using the NIFTI coordinate system. Rotation matrices created by AFNI and FSL, which use other coordinate systems, are converted when they are loaded. How rotation matrices are interpreted is defined using the -pkg option.\n* All rotation matrices are assumed to be transformations from the original data to a reference volume, except for AFNI rotation matrices, which are assumed to be transformations from the reference (i.e., target) to the original data."


#endif
