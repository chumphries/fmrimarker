/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of SVGGraph.
*
* SVGGraph is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* SVGGraph is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with SVGGraph.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file svggraph.h
  \author Colin Humphries
  \brief Base class for SVG graphs.
*/
#ifndef _SVGGRAPH_H
#define _SVGGRAPH_H

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <cmath>
#include <iomanip>

#include "svgdoc.h"

#define RNDERROR 0.001

namespace svggraph {
  
  /*! \class SVGGraph
    \brief SVG Graph Class
    
    This class will render a basic graph with axes, tickmarks, and labels in SVG format.
    
    \author Colin Humphries
  */
  class SVGGraph : public xmlgen::SVGTag {
  public:
    /*!
      Class constructor.
    */
    SVGGraph() {
      
      xlimauto = true;
      ylimauto = true;
      
      setWidth(600);
      setHeight(400);
      
      border = false;
      border_width = 1.0;
      border_color = "black";
      
      background = false;
      background_color = "none";
      graph_color = "none";
      
      pad_left = -1.0;
      pad_right = -1.0;
      pad_top = -1.0;
      pad_bottom = -1.0;
      
      graph_border_top = true;
      graph_border_bottom = true;
      graph_border_left = true;
      graph_border_right = true;
      graph_border_color = "black";
      graph_border_width = 1.0;
      
      SetFontAll("Arial");
      SetFontColorAll("black");
      
      title_fontsize = 12.0;
      title_spacing = -1.0;
      
      label_fontsize = 12.0;
      xlabel_spacing = -1.0;
      ylabel_spacing = -1.0;
      
      xtick = true;
      ytick = true;
      xtick_inset = 5.5;
      ytick_inset = 5.5;
      xtick_outset = 0.0;
      ytick_outset = 0.0;
      xticklabel = true;
      yticklabel = true;
      tick_width = 1.0;
      xticklabel_spacing = -1.0;
      yticklabel_spacing = -1.0;
      ticklabel_fontsize = 12.0;
      xtickvalauto = true;
      ytickvalauto = true;
      xticklabauto = true;
      yticklabauto = true;
      
      xgrid = false;
      ygrid = false;
      xgrid_color = "rgb(200,200,200)";
      ygrid_color = "rgb(200,200,200)";
      xgrid_width = 1.0;
      ygrid_width = 1.0;
      
      usexbase = false;
      useybase = false;
      xbaseval = 0.0;
      ybaseval = 0.0;
      xbase_color = "black";
      ybase_color = "black";
      xbase_width = 1.0;
      ybase_width = 1.0;
    }
    /*!
      Set the font for all labels
      \param[in] string fontname
    */
    void SetFontAll(const std::string &fn) {
      title_font = fn;
      label_font = fn;
      ticklabel_font = fn;
    }
    /*!
      Set the font color for all labels
      \param[in] string font color
    */
    void SetFontColorAll(const std::string &fc) {
      title_color = fc;
      xlabel_color = fc;
      ylabel_color = fc;
      xticklabel_color = fc;
      yticklabel_color = fc;
    }
    /*!
      This function should be called before Render(). It calculates the limits of the graph
      and the tickmarks/labels for the axes based on the input data.
    */
    virtual void Plot() {
      SetupLimits();
      PlotAxis();      
    }
    /*!
      Calculate the minimum and maximum x and y values.
      Setup tickmarks and labels.
    */
    void SetupLimits() {
      // Calculate data min and max and optimal tick mark spacing
      
      CalculateMinMax();
      CalculateTickSpacing();
      // Create the xaxis and yaxis tick mark labels
      if (xtickvalauto) {
	GetTickValues(xtickvalues,xmin,xmax,dxtick);
      }
      if (ytickvalauto) {
	GetTickValues(ytickvalues,ymin,ymax,dytick);
      }
      if (xticklabauto) {
	GetTickLabels(xticklabels,xtickvalues,xmaxticklabels);
      }
      if (yticklabauto) {
	GetTickLabels(yticklabels,ytickvalues,ymaxticklabels);
      }
      CalculatePosition();
      // std::cerr << xtickvalues[0] << std::endl;
    }
    /*!
      Create the graph elements.
    */
    void PlotAxis() {
      xmlgen::XTag *pxt;
      std::string ststr;
      
      // Background and border
      if (border || background) {
	// Place an extra rectangle
	pxt = addRect(0,0,width,height);
	ststr = "fill:" + background_color + ";";
	if (border) {
	  ststr += "stroke:" + border_color + ";";
	  ststr += "stroke-width:" + std::to_string(border_width) + ";";
	}
	pxt->addAttribute("style",ststr);
      }
      
      // Graph box
      ststr = "stroke:" + graph_border_color + ";stroke-width:" + std::to_string(graph_border_width) + ";";
      if (graph_border_top && graph_border_bottom &&
	  graph_border_left && graph_border_right) {
	pxt = addRect(graphx,graphy,graphw,graphh);
	std::string sstmp = ststr + "fill:" + graph_color + ";";
	pxt->addAttribute("style",sstmp);
      }
      else {
	if (!(graph_color.compare("none") == 0)) {
	  pxt = addRect(graphx,graphy,graphw,graphh);
	  std::string sstmp = "stroke:none;fill:" + graph_color + ";";
	  pxt->addAttribute("style",sstmp);
	}
	if (graph_border_top) {
	  pxt = addLine(graphx,graphy,graphx+graphw,graphy);
	  pxt->addAttribute("style",ststr);
	}
	if (graph_border_bottom) {
	  pxt = addLine(graphx,graphy+graphh,graphx+graphw,graphy+graphh);
	  pxt->addAttribute("style",ststr);
	}
	if (graph_border_left) {
	  pxt = addLine(graphx,graphy,graphx,graphy+graphh);
	  pxt->addAttribute("style",ststr);
	}
	if (graph_border_right) {
	  pxt = addLine(graphx+graphw,graphy,graphx+graphw,graphy+graphh);
	  pxt->addAttribute("style",ststr);
	}
      }

      // Title
      if (title.size()) {
	ststr = "text-anchor:middle;font-family:" + title_font +
	  ";font-size:" + std::to_string(title_fontsize) + ";stroke:none;fill:" + title_color + ";";
	// pxt = addText(graphx+graphw/2,graphy-0.8*title_fontsize,title);
	pxt = addText(graphx+graphw/2,4.0+title_fontsize,title);
	pxt->addAttribute("style",ststr);
      }
      
      // XLabel
      if (xlabel.size()) {
	ststr = "text-anchor:middle;font-family:" + label_font +
	  ";font-size:" + std::to_string(label_fontsize) + ";stroke:none;fill:" + xlabel_color + ";";
	pxt = addText(graphx+graphw/2,(double)height-0.5*label_fontsize,xlabel);
	pxt->addAttribute("style",ststr);
      }
      
      if (ylabel.size()) {

	ststr = "text-anchor:middle;font-family:" + label_font +
	  ";font-size:" + std::to_string(label_fontsize) + ";stroke:none;fill:" + ylabel_color + ";";
	double xrotpnt = graphx-6.0-0.2*ticklabel_fontsize-0.64*ymaxticklabels*ticklabel_fontsize-0.2*label_fontsize;
	pxt = addText(xrotpnt,graphy+graphh/2,ylabel);
	std::ostringstream posstr;
	posstr << "rotate(270," << xrotpnt << "," << graphy+graphh/2 << ")";
	pxt->addAttribute("transform",posstr.str());
	pxt->addAttribute("style",ststr);
      }

      // Axis tick marks and tick labels
      double newiipos;

      
      // First plot grid lines
      if (xgrid) {
	ststr = "stroke:" + xgrid_color + ";stroke-width:" + std::to_string(xgrid_width) + ";";
	for (std::vector<double>::const_iterator it=xtickvalues.begin(); it != xtickvalues.end(); ++it) {
	  if ((fabs(*it-xmin) < RNDERROR*dxtick) || (fabs(*it-xmax) < RNDERROR*dxtick)) {
	    continue;
	  }
	  newiipos = ((*it - xmin) / (xmax - xmin)) * graphw + graphx;
	  pxt = addLine(newiipos,graphy,newiipos,graphy+graphh);
	  pxt->addAttribute("style",ststr);
	}
	
      }
 
      if (ygrid) {
	ststr = "stroke:" + ygrid_color + ";stroke-width:" + std::to_string(ygrid_width) + ";";
	for (std::vector<double>::const_iterator it=ytickvalues.begin(); it != ytickvalues.end(); ++it) {
	  if ((fabs(*it-ymin) < RNDERROR*dytick) || (fabs(*it-ymax) < RNDERROR*dytick)) {
	    continue;
	  }
	  newiipos = (1 - (*it - ymin) / (ymax - ymin)) * graphh + graphy;
	  pxt = addLine(graphx,newiipos,graphx+graphw,newiipos);
	  pxt->addAttribute("style",ststr);
	}
      }

      if ((xtick || xticklabel) && (graph_border_top || graph_border_bottom)) {
	std::string ststr1 = "text-anchor:middle;font-family:" + ticklabel_font +
	  ";font-size:" + std::to_string(ticklabel_fontsize) + ";stroke:none;fill:" + xticklabel_color + ";";
	std::string ststr2 = "stroke:" + xticklabel_color + ";stroke-width:" + std::to_string(tick_width) + ";";
	uint labcount=0;
	for (std::vector<double>::const_iterator it=xtickvalues.begin(); it != xtickvalues.end(); ++it) {
	  // if ((fabs(*it-xmin) < 0.0001*dxtick) || (fabs(*it-xmax) < 0.0001*dxtick)) {
	  //   continue;
	  // }
	  newiipos = ((*it - xmin) / (xmax - xmin)) * graphw + graphx;
	  if (xticklabel && xticklabels.size()>0) {
	    pxt = addText(newiipos,graphy+graphh+1.0*ticklabel_fontsize+2.0,xticklabels[labcount]);
	    pxt->addAttribute("style",ststr1);
	    ++labcount;
	    if (labcount == xticklabels.size()) {
	      labcount = 0;
	    }
	  }
	  
	  // Don't plot gridlines or tickmarks if we are at the edge of the graph
	  if (graph_border_right && (fabs(*it-xmax) < RNDERROR*dxtick)) {
	    continue;
	  }      
	  if (graph_border_left && (fabs(*it-xmin) < RNDERROR*dxtick)) {
	    continue;
	  }      
	  if ((xtick_inset > 0.0 || xtick_outset > 0.0) && xtick) {
	    if (graph_border_bottom) {
	      pxt = addLine(newiipos,graphy+graphh+xtick_outset,newiipos,graphy+graphh-xtick_inset);
	      pxt->addAttribute("style",ststr2);
	    }
	    if (graph_border_top) {
	      pxt = addLine(newiipos,graphy-xtick_outset,newiipos,graphy+xtick_inset);
	      pxt->addAttribute("style",ststr2);
	    }
	  }
	  
	}

	
	
      }

      if ((ytick || yticklabel) && (graph_border_left || graph_border_right)) {
	std::string ststr1 = "text-anchor:end;font-family:" + ticklabel_font +
	  ";font-size:" + std::to_string(ticklabel_fontsize) + ";stroke:none;fill:" + xticklabel_color + ";";
	std::string ststr2 = "stroke:" + xticklabel_color + ";stroke-width:" + std::to_string(tick_width) + ";";
	uint labcount=0;
	for (std::vector<double>::const_iterator it=ytickvalues.begin(); it != ytickvalues.end(); ++it) {
	  newiipos = (1 - (*it - ymin) / (ymax - ymin)) * graphh + graphy;
	  if (yticklabel && yticklabels.size()>0) {
	    pxt = addText(graphx-2.0-0.2*label_fontsize,
			  newiipos+.40*label_fontsize,yticklabels[labcount]);
	    pxt->addAttribute("style",ststr1);
	    ++labcount;
	    if (labcount == yticklabels.size()) {
	      labcount = 0;
	    }
	  }
	  // Don't plot gridlines or tickmarks if we are at the edge of the graph
	  if (graph_border_top && (fabs(*it-ymax) < RNDERROR*dytick)) {
	    continue;
	  }
	  if (graph_border_bottom && (fabs(*it-ymin) < RNDERROR*dytick)) {
	    continue;
	  }
	  if ((ytick_inset > 0.0 || ytick_outset > 0.0) && ytick) {
	    if (graph_border_right) {
	      pxt = addLine(graphx+graphw-ytick_inset,newiipos,graphx+graphw+ytick_outset,newiipos);
	      pxt->addAttribute("style",ststr2);
	    }
	    if (graph_border_left) {
	      pxt = addLine(graphx-ytick_outset,newiipos,graphx+ytick_inset,newiipos);
	      pxt->addAttribute("style",ststr2);
	    }
	  }
	  
	}
      }
      
      if (usexbase || useybase) {
	double xz = xbaseval;
	double yz = ybaseval;
	ConvertPnts(xz,yz);
	if (usexbase) {
	  pxt = addLine(xz,graphy,xz,graphy+height);
	  pxt->addAttribute("style","stroke:" + xbase_color +
			    ";stroke-width:" + std::to_string(xbase_width)+";");
	}
	if (useybase) {
	  pxt = addLine(graphx,yz,graphx+width,yz);
	  pxt->addAttribute("style","stroke:" + ybase_color +
			    ";stroke-width:" + std::to_string(ybase_width)+";");
	}
      }
      
    }

    void setPadLeft(double pv) {pad_left = pv;}
    void setPadRight(double pv) {pad_right = pv;}
    void setPadTop(double pv) {pad_top = pv;}
    void setPadBottom(double pv) {pad_bottom = pv;}
    
    void Border(bool tog) {border = tog;}
    void setBorderColor(const std::string &tstr) {border_color = tstr;}
    void setBorderWidth(double tval) {border_width = tval;}
    
    void setBorder(double bw, const std::string &bc) {
      border_width = bw;
      border_color = bc;
      border = true;
      background = true;
    }
    
    void setBackground(const std::string &bc) {
      background_color = bc;
      background = true;
    }
    
    void setGraphBorder(bool bt, bool bb, bool bl, bool br) {
      graph_border_top = bt;
      graph_border_bottom = bb;
      graph_border_left = bl;
      graph_border_right = br;
    }
    void setGraphColor(const std::string &tstr) {graph_color = tstr;}
    
    void XGrid(bool tog) {xgrid = tog;}
    void YGrid(bool tog) {ygrid = tog;}
    void setXGridColor(const std::string &tstr) { xgrid_color = tstr;}
    void setYGridColor(const std::string &tstr) { ygrid_color = tstr;}
    void setXGridWidth(double tval) {xgrid_width=tval;}
    void setYGridWidth(double tval) {ygrid_width=tval;}
    
  void setTitle(const std::string &tstr) { title = tstr;}
  void setTitleFont(const std::string &tstr) { title_font = tstr;}
  void setTitleColor(const std::string &tstr) { title_color = tstr;}
  void setTitleFontSize(double tval) { title_fontsize = tval;}
  void setXLabel(const std::string &tstr) { xlabel = tstr;}
  void setYLabel(const std::string &tstr) { ylabel = tstr;}
  void setLabelFont(const std::string &tstr) { label_font = tstr;}
  void setLabelFontSize(double tval) { label_fontsize = tval;}
  void setXLabelColor(const std::string &tstr) { xlabel_color = tstr;}
  void setYLabelColor(const std::string &tstr) { ylabel_color = tstr;}
  void setXlim(double xmn, double xmx) {xmin = xmn; xmax = xmx; xlimauto=false;}
  void setYlim(double ymn, double ymx) {ymin = ymn; ymax = ymx; ylimauto=false;}

  void XBase(bool tog) {usexbase = tog;}
  void YBase(bool tog) {useybase = tog;}
  void XBase(double val) {usexbase = true; xbaseval=val;}
  void YBase(double val) {useybase = true; ybaseval=val;}
  void setXBaseWidth(double tval) {xbase_width=tval;}
  void setYBaseWidth(double tval) {ybase_width=tval;}
  void setXBaseColor(const std::string &tstr) {xbase_color=tstr;}
  void setYBaseColor(const std::string &tstr) {ybase_color=tstr;}

  void XTick(bool tog) {xtick = tog;}
  void YTick(bool tog) {ytick = tog;}
  void XTickLabel(bool tog) {xticklabel = tog;}
  void YTickLabel(bool tog) {yticklabel = tog;}
  void setXTickInset(double tval) {xtick_inset=tval;}
  void setXTickOutset(double tval) {xtick_outset=tval;}
  void setYTickInset(double tval) {ytick_inset=tval;}
  void setYTickOutset(double tval) {ytick_outset=tval;}
  void setXTickValues(const std::vector<double> &tval) {xtickvalues = tval; xtickvalauto=false;}
  void setYTickValues(const std::vector<double> &tval) {ytickvalues = tval; ytickvalauto=false;}
    void setXTickLabels(const std::vector<std::string> &tval) {xticklabels = tval; xticklabauto=false;}
    void setYTickLabels(const std::vector<std::string> &tval) {yticklabels = tval; yticklabauto=false;}
  
  void ConvertPnts(std::vector<double> &xpos, std::vector<double> &ypos) {
    for (std::vector<double>::iterator it = xpos.begin(); it != xpos.end(); ++it) {
      *it = ((*it - xmin) / (xmax - xmin)) * graphw + graphx;
    }
    for (std::vector<double>::iterator it = ypos.begin(); it != ypos.end(); ++it) {
      *it = (1 - (*it - ymin) / (ymax - ymin)) * graphh + graphy;
    }
  }
  void ConvertPnts(double &xpos, double &ypos) {
    xpos = ((xpos - xmin) / (xmax - xmin)) * graphw + graphx;
    ypos = (1 - (ypos - ymin) / (ymax - ymin)) * graphh + graphy;
  }
  protected:
    virtual void CalculateMinMax() {
      
      if (!xlimauto && !ylimauto) {
	return;
      }
      if (xlimauto) {
	xmin = 0.0;
	xmax = 1.0;
      }
      if (ylimauto) {
	ymin = 0.0;
	ymax = 1.0;
      }
      
    }
    void CalculateTickSpacing() {
      // Should add something here to check ratio
      // of fontsize to graphw/graphh
      double q;
      q = floor(log10((xmax-xmin)/11.0));
      dxtick = ((xmax-xmin)/11.0)/pow(10,q);
      if (dxtick < 2.0) {
	dxtick = 2.0 * pow(10,q);
      }
      else if (dxtick < 5.0) {
	dxtick = 5.0 * pow(10,q);
      }
      else {
	dxtick = 10.0 * pow(10,q);
      }
      q = floor(log10((ymax-ymin)/11.0));
      dytick = ((ymax-ymin)/11.0)/pow(10,q);
      if (dytick < 2.0) {
	dytick = 2.0 * pow(10,q);
      }
      else if (dytick < 5.0) {
	dytick = 5.0 * pow(10,q);
      }
      else {
	dytick = 10.0 * pow(10,q);
      }
    }
    
    void CalculatePosition() {
      if (pad_left < 0.0) {
	// cerr << ymaxticklabels << endl;
	if (ymaxticklabels > 4) {
	  graphx = ymaxticklabels*ticklabel_fontsize*0.64 + 7.0;
	}
	else {
	  graphx = 4*ticklabel_fontsize*0.64 + 7.0;
	}
	if (ylabel.size()) {
	  // graphx += 1.6*label_fontsize;
	  graphx += 1.2*label_fontsize + 2.0;
	}
      }
      else {
	graphx = pad_left;
      }
      // graphx = 4*ticklabel_fontsize;
      if (pad_right < 0.0) {
	graphw = (double)width - graphx - 7.0; // 1.0*ticklabel_fontsize;
      }
      else {
	graphw = (double)width - graphx - pad_right;
      }
      if (pad_top < 0.0) {
	if (title.size()) {
	  graphy = 4.0 + 1.2*title_fontsize + 4.0; // 2.6*title_fontsize;
	}
	else {
	  graphy = 7.0; // ticklabel_fontsize;
	}
      }
      else {
	graphy = pad_top;
      }
      if (pad_bottom < 0.0) {
	// graphh = (double)height - graphy - 0.0*ticklabel_fontsize;
	graphh = (double)height - graphy - 2.0;
	if (yticklabel) {
	  graphh -= 1.2*ticklabel_fontsize;
	}
	if (xlabel.size()) {
	  graphh -= 1.5*label_fontsize + 1.0;
	}
      }
      else {
	graphh = (double)height - graphy - pad_bottom;
      }
    }
    
    void GetTickValues(std::vector<double> &tvalues, double dmin, 
		       double dmax, double dtick) {
      tvalues.clear();
      int imin, imax;
      imin = (int)round(dmin/dtick);
      if (((double)imin * dtick) < (dmin - (dtick*RNDERROR))) {
	++imin;
      }
      imax = (int)round(dmax/dtick);
      if (((double)imax * dtick) > (dmax + (dtick*RNDERROR))) {
	--imax;
      }
      for (int ii=imin; ii<= imax; ++ii) {
	tvalues.push_back((double)ii * dtick);
      }
    }
    void GetTickLabels(std::vector<std::string> &tlabels, const std::vector<double> &tvalues,
		       double &maxsize) {
      tlabels.clear();
      std::string tstr;
      double tsize;
      maxsize = 0;
      for (std::vector<double>::const_iterator it=tvalues.begin();
	   it != tvalues.end(); ++it) {
	std::ostringstream posstr;
	posstr << std::setprecision(6) << *it;
	tstr = posstr.str();
	if (tstr[0] == '0' && tstr[1] == '.') {
	  tstr = tstr.substr(1,std::string::npos);
	}
	if (tstr[0] == '-' && tstr[1] == '0' && tstr[2] == '.') { 
	  tstr = tstr.substr(1,std::string::npos);
	  tstr[0] = '-';
	}
	tsize = tstr.size();
	if (tstr.find_first_of('.') != std::string::npos) {
	  tsize -= 0.5;
	}
	if (tsize > maxsize) {
	  maxsize = tsize;
	}
	tlabels.push_back(tstr);
      }
 
    }
    double xmin;
    double ymin;
    double xmax;
    double ymax;
    bool xlimauto;
    bool ylimauto;
    double graphx;
    double graphy;
    double graphw;
    double graphh;
  private:

    double dxtick;
    double dytick;
  
    double xmaxticklabels;
    double ymaxticklabels;
 
    std::vector<std::string> xticklabels;
    std::vector<std::string> yticklabels;
    std::vector<double> xtickvalues;
    std::vector<double> ytickvalues;
    bool xtickvalauto;
    bool ytickvalauto;
    bool xticklabauto;
    bool yticklabauto;
  
    double pad_left;
    double pad_right;
    double pad_top;
    double pad_bottom;

    bool border;
    double border_width;
    std::string border_color;
	
    bool background;
    std::string background_color;
    std::string graph_color;

    bool graph_border_top;
    bool graph_border_bottom;
    bool graph_border_left;
    bool graph_border_right;
    std::string graph_border_color;
    double graph_border_width;
  
    std::string title;
    std::string title_font;
    std::string title_color;
    double title_fontsize;
    double title_spacing;
  
    std::string xlabel;
    std::string ylabel;
    std::string label_font;
    std::string xlabel_color;
    std::string ylabel_color;
    double label_fontsize;
    double xlabel_spacing;
    double ylabel_spacing;

    bool xtick;
    bool ytick;
    double xtick_inset;
    double xtick_outset;
    double ytick_inset;
    double ytick_outset;
    bool xticklabel;
    bool yticklabel;
    double tick_width;
    double xticklabel_spacing;
    double yticklabel_spacing;
    std::string ticklabel_font;
    std::string xticklabel_color;
    std::string yticklabel_color;
    double ticklabel_fontsize;
 
  
    bool xgrid;
    bool ygrid;
    std::string xgrid_color;
    std::string ygrid_color;
    std::string xgrid_type;
    std::string ygrid_type;
    double xgrid_width;
    double ygrid_width;
    bool usexbase;
    bool useybase;
    double xbaseval;
    double ybaseval;
    double xbase_width;
    double ybase_width;
    std::string xbase_color;
    std::string ybase_color;
  
  };

}

#endif
