/*********************************************************************
* Copyright 2015, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file mreport.h
  \author Colin Humphries
  \brief Classes for creating an HTML report about subject motion.
*/
#ifndef _MREPORT_H
#define _MREPORT_H

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <cmath>
#include <iomanip>
#include <ctime>
#include "mrirotate.h"
// #include "xmldoc.h"
#include "svgdoc.h"
#include "htmldoc.h"
#include "svglinegraph.h"
#include "argparser.h"

// using namespace std;

/*! \class MReport
  \brief Base motion report class.

  This class is used to define an HTML object for displaying information about subject motion.

  \author Colin Humphries
*/
class MReport : public xmlgen::HTMLTag {
public:
  /*!
    Class constructor.
  */
  MReport() {
    graphwidth = 830;
    graphheight = 188;

    xmlgen::XTag *xtpnt = phead->addHEmptyElement("meta");
    xtpnt->addAttribute("name","generator");
    xtpnt->addAttribute("content","FMRIMarker");
    pcss = new xmlgen::CSSStyleTag();
    pcss->addDeclaration("body","font-family","Arial");
    // pcss->addDeclaration("body","background-color","rgb(238,235,245)");
    pcss->addDeclaration("body","background-color","rgb(255,255,255)");
    pcss->addDeclaration("h1","font-size","26px");
    // pcss->addDeclaration("h1","margin","0px");
    // pcss->addDeclaration("h1","margin-bottom","7px");
    // pcss->addDeclaration("h2","font-size","16px");
    pcss->addDeclaration(".basediv","width","850px");
    pcss->addDeclaration(".basediv","margin","auto");    
    pcss->addDeclaration(".basediv","margin-top","25px");    
    pcss->addDeclaration(".basediv","margin-bottom","25px");    
    pcss->addDeclaration(".basediv p","margin-top","2px");    
    pcss->addDeclaration(".basediv p","margin-bottom","6px");
    pcss->addDeclaration(".header","border-bottom","1px solid black");
    pcss->addDeclaration(".header","margin-bottom","20px");
    pcss->addDeclaration(".footer","border-top","1px solid black");
    pcss->addDeclaration(".footer","margin-top","20px");
    pcss->addDeclaration(".footer","padding-top","10px");

    /*
    pcss->addDeclaration(".mgraphs","border","1px solid black");    
    pcss->addDeclaration(".mgraphs","margin-top","15px");    
    pcss->addDeclaration(".mgraphs","padding","15px");    
    pcss->addDeclaration(".mgraphs","background-color","rgb(255,255,255)");
    pcss->addDeclaration(".mgraphs","border-radius","9px");
    pcss->addDeclaration(".headdiv","border","1px solid black");    
    pcss->addDeclaration(".headdiv","margin-top","15px");    
    pcss->addDeclaration(".headdiv","padding","15px");    
    pcss->addDeclaration(".headdiv","background-color","rgb(255,255,255)");
    pcss->addDeclaration(".headdiv","border-radius","9px");
    */
    phead->addElement(pcss);

    pmbase = pbody->addElement("div");
    pmbase->addAttribute("class","basediv");
    pmheader = pmbase->addElement("div");
    pmheader->addAttribute("class","header");
    pmcontent = pmbase->addElement("div");
    pmcontent->addAttribute("class","content");
    pmfooter = pmbase->addElement("div");
    pmfooter->addAttribute("class","footer");

  }
  /*!
    Add motion graphs of the rotation parameters
    \param[in] rp vector of rotation parameter structures
    \param[in,out] ptag pointer of parent xtag object
   */
  void AddMotionGraphs(const std::vector<RParameters> &rp, xmlgen::XTag *ptag) {
    std::vector<double> ldata;
    double bl;
    SVGLineGraph *transgraph = new SVGLineGraph();
    SVGLineGraph *rotgraph = new SVGLineGraph();
    
    bl = rp[0].xtrans;
    for (std::vector<RParameters>::const_iterator it=rp.begin();
	 it != rp.end(); ++it) {
      ldata.push_back(it->xtrans - bl);
    }
    transgraph->addLineData(ldata);
    ldata.clear();
    bl = rp[0].ytrans;
    for (std::vector<RParameters>::const_iterator it=rp.begin();
	 it != rp.end(); ++it) {
      ldata.push_back(it->ytrans - bl);
    }
    transgraph->addLineData(ldata);
    ldata.clear();
    bl = rp[0].ztrans;
    for (std::vector<RParameters>::const_iterator it=rp.begin();
	 it != rp.end(); ++it) {
      ldata.push_back(it->ztrans - bl);
    }
    transgraph->addLineData(ldata);
    ldata.clear();
    transgraph->setWidth(graphwidth);
    transgraph->setHeight(graphheight);
    transgraph->setTitle("Translation");

    bl = rp[0].roll;
    for (std::vector<RParameters>::const_iterator it=rp.begin();
	 it != rp.end(); ++it) {
      ldata.push_back(it->roll - bl);
    }
    rotgraph->addLineData(ldata);
    ldata.clear();
    bl = rp[0].pitch;
    for (std::vector<RParameters>::const_iterator it=rp.begin();
	 it != rp.end(); ++it) {
      ldata.push_back(it->pitch - bl);
    }
    rotgraph->addLineData(ldata);
    ldata.clear();
    bl = rp[0].yaw;
    for (std::vector<RParameters>::const_iterator it=rp.begin();
	 it != rp.end(); ++it) {
      ldata.push_back(it->yaw - bl);
    }
    rotgraph->addLineData(ldata);
    // ldata.clear();
    rotgraph->setWidth(graphwidth);
    rotgraph->setHeight(graphheight);
    rotgraph->setTitle("Rotation");

    ptag->addElement(transgraph);
    ptag->addElement(rotgraph);

    transgraph->Plot();
    rotgraph->Plot();
  }
  /*!
    Add framewise displacement graph using rotation parameters
    \param[in] rp vector of rotation parameter structures
    \param[in,out] ptag pointer of parent xtag object
   */
  void AddFDGraph(const std::vector<RParameters> &rp, xmlgen::XTag *ptag) {
    std::vector<double> ldata;
    SVGLineGraph *fdgraph = new SVGLineGraph();
    MRIRotate mrot;
    mrot.FD(rp,ldata);
    fdgraph->addLineData(ldata);
    fdgraph->setWidth(graphwidth);
    fdgraph->setHeight(graphheight);
    fdgraph->setTitle("Framewise Displacement");
    ptag->addElement(fdgraph);
    fdgraph->Plot();
  }

protected:
  xmlgen::CSSStyleTag *pcss;
  xmlgen::XTag *pmbase;
  xmlgen::XTag *pmheader;
  xmlgen::XTag *pmcontent;
  xmlgen::XTag *pmfooter;
  int graphwidth;
  int graphheight;
};


class MotionReport : public MReport {
public:
  MotionReport() {
    xmlgen::XTag *ptag;
    ptag = pmheader->addShortElement("h1");
    ptag->addString("FMRI Motion Report");
    
    ptag = pmfooter->addShortElement("p");
    ptag->addString("Generated by FMRIMReport on ");
    time_t now = time(0);
    char *dt = ctime(&now);
    ptag->addString(dt);

    pcss->addDeclaration(".rundiv","margin-top","5px");    
    pcss->addDeclaration(".rundiv","margin-bottom","0px");    
    pcss->addDeclaration(".rundiv","padding","10px");    
    // pcss->addDeclaration(".rundiv","border-top","1px solid black");    
    pcss->addDeclaration(".rundiv","border-bottom","1px solid black");    
    // pcss->addDeclaration(".rundiv","background-color","rgb(255,255,255)");
    // pcss->addDeclaration(".rundiv","border-radius","9px");
    pcss->addDeclaration(".footer","border-top","none");
    pcss->addDeclaration(".footer","margin-top","0px");
  }

  void AddRun(const std::vector<RParameters> &rp,
	      const std::string &filename) {
    xmlgen::XTag *ptag1, *ptag2;
    ptag1 = pmcontent->addElement("div");
    ptag1->addAttribute("class","rundiv");
    ptag2 = ptag1->addShortElement("p");
    ptag2->addString("File: ");
    ptag2->addString(filename);
    ptag2 = ptag1->addShortElement("p");
    ptag2->addString("Frame Orientation Count: ");
    MRIRotate mrot;
    ptag2->addString(std::to_string(mrot.FOC(rp)));
    AddMotionGraphs(rp,ptag1);
    AddFDGraph(rp,ptag1);
  }
  void AddSubjectInfo(argparser::ArgParser &parg) {
    std::string expid, subjid, sessionid, runid;
    bool isfound = false;
    parg.Get("experiment",expid);
    isfound = isfound || parg.Found();
    parg.Get("subject",subjid);
    isfound = isfound || parg.Found();
    parg.Get("session",sessionid);
    isfound = isfound || parg.Found();
    parg.Get("run",runid);
    isfound = isfound || parg.Found();
    if (isfound) {
      xmlgen::XTag *pbtag, *ptag;
      pbtag = pmcontent->addElement("div");
      pbtag->addAttribute("class","rundiv");
      if (!expid.empty()) {
	ptag = pbtag->addShortElement("p");
	ptag->addString("Experiment: ");
	ptag->addString(expid);
      }
      if (!subjid.empty()) {
	ptag = pbtag->addShortElement("p");
	ptag->addString("Subject: ");
	ptag->addString(subjid);
      }
      if (!sessionid.empty()) {
	ptag = pbtag->addShortElement("p");
	ptag->addString("Session: ");
	ptag->addString(sessionid);
      }
      if (!runid.empty()) {
	ptag = pbtag->addShortElement("p");
	ptag->addString("Run: ");
	ptag->addString(runid);
      }
    }
  }
};

class MarkerReport : public MReport {
public:
  MarkerReport() {
    xmlgen::XTag *ptag;
    ptag = pmheader->addShortElement("h1");
    ptag->addString("FMRIMarker Report");
    
    ptag = pmfooter->addShortElement("p");
    ptag->addString("Generated by FMRIMarker on ");
    time_t now = time(0);
    char *dt = ctime(&now);
    ptag->addString(dt);
  }

  void AddInfo(double sfwhm, double tfwhm,
	       const std::string &infile, const std::string &matfile) {
    xmlgen::XTag *pbtag, *ptag;
    pbtag = pmcontent->addElement("div");
    ptag = pbtag->addShortElement("p");
    ptag->addString("Input File: ");
    ptag->addString(infile);
    ptag = pbtag->addShortElement("p");
    ptag->addString("Matrix File: ");
    ptag->addString(matfile);
    ptag = pbtag->addShortElement("p");
    ptag->addString("Spatial FWHM: ");
    ptag->addString(std::to_string(sfwhm));
    ptag = pbtag->addShortElement("p");
    ptag->addString("Temporal FWHM: ");
    ptag->addString(std::to_string(tfwhm));
    


  }

  void AddSubjectInfo(argparser::ArgParser &parg) {
    std::string expid, subjid, sessionid, runid;
    bool isfound = false;
    parg.Get("experiment",expid);
    isfound = isfound || parg.Found();
    parg.Get("subject",subjid);
    isfound = isfound || parg.Found();
    parg.Get("session",sessionid);
    isfound = isfound || parg.Found();
    parg.Get("run",runid);
    isfound = isfound || parg.Found();
    if (isfound) {
      xmlgen::XTag *pbtag, *ptag;
      pbtag = pmcontent->addElement("div");
      if (!expid.empty()) {
	ptag = pbtag->addShortElement("p");
	ptag->addString("Experiment: ");
	ptag->addString(expid);
      }
      if (!subjid.empty()) {
	ptag = pbtag->addShortElement("p");
	ptag->addString("Subject: ");
	ptag->addString(subjid);
      }
      if (!sessionid.empty()) {
	ptag = pbtag->addShortElement("p");
	ptag->addString("Session: ");
	ptag->addString(sessionid);
      }
      if (!runid.empty()) {
	ptag = pbtag->addShortElement("p");
	ptag->addString("Run: ");
	ptag->addString(runid);
      }
    }
  }

  void AddMATData(const std::vector<fmri::OrientationMatrix> &matdata,
		  const fmri::MRIBase &base) {
    std::vector<RParameters> rp;
    MRIRotate mrot;

    mrot.MAT2PAR(matdata,rp,base,base);
    AddMotionGraphs(rp,pmcontent);
    AddFDGraph(rp,pmcontent);
  }

  void AddMATData(const std::vector<RParameters> &rp) {
    AddMotionGraphs(rp,pmcontent);
    AddFDGraph(rp,pmcontent);
  }

  void AddTCData(const fmri::MRIData<double> &data,
		 const fmri::MRIData<double> &model, uint skip) {
    std::vector<double> dvars1, dvars2;
    double dmx1, dmx2;
    // Original data
    dmx1 = DVars(data,dvars1,skip);
    dmx2 = DVars(model,dvars2,skip);
    if (dmx2 > dmx1) {
      dmx1 = dmx2;
    }
    SVGLineGraph *pgraph1 = new SVGLineGraph();
    pgraph1->addLineData(dvars1);
    pgraph1->addLineData(dvars2);
    pgraph1->setWidth(graphwidth);
    pgraph1->setHeight(graphheight);
    pgraph1->setTitle("DVars (Before/After)");
    pmcontent->addElement(pgraph1);
    pgraph1->setYlim(0.0,dmx1);
    pgraph1->Plot();

    /*
    // Corrected data
    SVGLineGraph *pgraph2 = new SVGLineGraph();
    pgraph2->addLineData(dvars2);
    pgraph2->setWidth(graphwidth);
    pgraph2->setHeight(graphheight);
    pgraph2->setTitle("DVars Corrected");
    pmcontent->addElement(pgraph2);
    pgraph2->setYlim(0.0,dmx1);
    pgraph2->Plot();
    */
  }

  double DVars(const fmri::MRIData<double> &data,
	       std::vector<double> &dvars, uint skip) {
    double dv;
    double dvm = 0.0;
    for (uint ii=0; ii<data.NumTimePnts(); ++ii) {
      
      dv = 0.0;
      if (ii >= skip+1) {
	for (uint jj=0; jj<data.NumVoxels(); ++jj) {
	  dv += (data(ii,jj)-data(ii-1,jj)) * (data(ii,jj)-data(ii-1,jj));
	}
	dv = sqrt(dv/(double)data.NumVoxels());
      }
      dvars.push_back(dv);
      if (dv > dvm) {
	dvm = dv;
      }
    }
    return dvm;
  }
};

#endif
