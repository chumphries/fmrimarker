/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file fmridatastats.cpp
  \author Colin Humphries
  \brief Fmridatastats main function.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include "argparser.h"
#include "mrivol.h"
#include "niftifile.h"
#include "version.h"

// using namespace std;

/*!
  \fn print_help
  \brief Print help message.

  This function prints a help message to stdout.
 */
void print_help(bool isshort, uint ignore) {
  using namespace std;
  using namespace argparser;
  HelpText ht;
  ostringstream ss(ios::ate);
  ht.SetFunctionName("fmridstats");
  ht.SetFunctionDescription("FMRI Data Statistics");
  ht.SetFunctionVersion(MARKER_VERSION_STRING);

  ht.AddArgument("in","Input dataset",1);
  ht.AddDescription("-in <filename>","The input is an fMRI time series in NIFTI format.");
  ht.AddArgument("out","Output data file",1);
  ht.AddDescription("-out <filename>","The output type depends on the command.");
  ht.AddArgument("mask","Mask",1);
  ht.AddArgument("mean","Voxelwise mean",0);
  ht.AddArgument("std","Voxelwise standard deviation",0);
  ht.AddArgument("var","Voxelwise variance",0);
  ht.AddArgument("skew","Voxelwise skewness",0);
  ht.AddArgument("kurt","Voxelwise kurtosis",0);
  ht.AddArgument("min","Voxelwise minimum",0);
  ht.AddArgument("max","Voxelwise maximum",0);

  ss.str("Number of initial volumes to ignore (default = ");
  ss << ignore << ")";
  ht.AddArgument("ignore",ss.str(),1);
  ht.AddArgument("double","Save data using 64-bit (instead of 32-bit) floating point numbers",0);
  ht.AddDescription("-double","All internal calculations are performed using 64-bit floating point numbers. By default, the program will save the output file using 32-bit floating point numbers. Use this option to save in 64-bit format.");

  ht.AddArgument("help","Print a detailed help message.",0);
  if (isshort) {
    cout << ht.ShortText() << endl;
  }
  else {
    cout << ht.LongText() << endl;
  }
}


/*!
  \fn main
  \brief Main function

  This is the main function for fmrimotionstats
 */
int main (int argc, const char **argv) {
  using namespace std;
  using namespace argparser;
  using namespace fmri;
  
  uint skip = 0;
  bool verbose = true;
  int niftidatatype = NIFTI_TYPE_FLOAT32;
  
  // Parse command line options
  if (argc < 2) {
    print_help(true,skip);
    return 0;
  }
  ArgParser parg(argv,argc);
  if (parg.Exists("help") || parg.Exists("h")) {
    print_help(false,skip);
    return 0;
  }

  string infile, outfile, maskfile;

  parg.Get("in",infile);
  if (!parg.Found() || parg.Error()) {
    cerr << "Error: No input file specified" << endl;
    return 1;
  }
  if (parg.Exists("double")) {
    niftidatatype = NIFTI_TYPE_FLOAT64;
  }
  // Initial volume skipping parameters
  parg.Get("ignore",skip);

  // Load data files //////////////////////////////////////////////////
  NIFTIFile nin;
  MRIData<double> data;
  // input fMRI time series
  if (!nin.Open(infile,'r')) {
    cerr << "Error opening input file" << endl;
    return 1;
  }
  if ((int)skip >= nin.Size(3)) {
    cerr << "Error: number of skipped time points is greater or equal to the total number of time points" << endl;
    return 1;
  }
  if (verbose) {
    cout << "Input file: " << infile << endl;
    cout << "Voxels: " << nin.Size(0) << " x " << nin.Size(1) << " x " << nin.Size(2) << endl;
    cout << "Time points: " << nin.Size(3) << endl;
  }

  parg.Get("mask",maskfile);
  if (parg.Found()) {
    NIFTIFile nin2;
    if (!nin2.Open(maskfile,'r')) {
      cerr << "Error opening mask file" << endl;
      return 1;
    }
    if ((nin2.XSize() != nin.XSize()) || 
        (nin2.YSize() != nin.YSize()) || 
        (nin2.ZSize() != nin.ZSize())) {
      cerr << "Error: mask volume must be the same size as input volume" << endl;
      return 1;
    }
    MRIVol<float> mask;
    if (!nin2.Read(mask)) {
      cerr << "Error reading mask file" << endl;
      return 1;
    }
    nin2.Close();
    if (!nin.Read(data,mask)) {
      cerr << "Error reading input data" << endl;
      return 1;
    }
  }
  else {
    if (!nin.Read(data)) {
      cerr << "Error reading input data" << endl;
      return 1;
    }

  }
  nin.Close();

  uint numvol = 0;
  vector<string> volcmds = {"mean","std","var","skew","kurt","min","max"};

  for (vector<string>::const_iterator it=volcmds.begin();it != volcmds.end(); ++it) {
    if (parg.Exists(*it)) {
      ++numvol;
    }
  }

  cout << numvol << endl;
  if (numvol > 0) {
    MRIData<double> results(data,numvol);
    size_t volindex = 0;

    if (parg.Exists("mean")) {
      for (uint ii=0; ii<data.NumVoxels();++ii) {
	results(volindex,ii) = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  results(volindex,ii) += data(jj,ii);
	}
	results(volindex,ii) /= (double)(data.NumTimePnts()-skip);
      }      
      ++volindex;
    }
    if (parg.Exists("std")) {
      double mm;
      for (uint ii=0; ii<data.NumVoxels();++ii) {
	mm = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  mm += data(jj,ii);
	}
	mm /= (double)(data.NumTimePnts()-skip);
	results(volindex,ii) = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  results(volindex,ii) += (data(jj,ii)-mm)*(data(jj,ii)-mm);
	}
	results(volindex,ii) /= (double)(data.NumTimePnts()-skip-1);
	results(volindex,ii) = sqrt(results(volindex,ii));
      }      
      ++volindex;
    }
    if (parg.Exists("var")) {
      double mm;
      for (uint ii=0; ii<data.NumVoxels();++ii) {
	mm = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  mm += data(jj,ii);
	}
	mm /= (double)(data.NumTimePnts()-skip);
	results(volindex,ii) = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  results(volindex,ii) += (data(jj,ii)-mm)*(data(jj,ii)-mm);
	}
	results(volindex,ii) /= (double)(data.NumTimePnts()-skip-1);
      }      
      ++volindex;
    }
    if (parg.Exists("skew")) {
      // Note: using matlab bias-corrected formula
      // https://www.mathworks.com/help/stats/skewness.html
      double mm, ss, nn;
      nn = (double)(data.NumTimePnts()-skip);
      if (nn < 3) {
	cerr << "Error: at least three time points are needed to calculate sample skewness" << endl;
	return 1;
      }
      for (uint ii=0; ii<data.NumVoxels();++ii) {
	mm = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  mm += data(jj,ii);
	}
	mm /= (double)(data.NumTimePnts()-skip);
	results(volindex,ii) = 0.0;
	ss = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  results(volindex,ii) += (data(jj,ii)-mm)*(data(jj,ii)-mm)*(data(jj,ii)-mm);
	  ss += (data(jj,ii)-mm)*(data(jj,ii)-mm);
	}
	ss = sqrt(ss / nn);
	results(volindex,ii) = (sqrt(nn*(nn-1))/(nn-1))*(results(volindex,ii)/nn) / (ss*ss*ss);
      }      
      ++volindex;
    }
    if (parg.Exists("kurt")) {
      // Note: using matlab bias-corrected formula
      // https://www.mathworks.com/help/stats/kurtosis.html
      double mm, ss, nn;
      nn = (double)(data.NumTimePnts()-skip);
      if (nn < 4) {
	cerr << "Error: at least four time points are needed to calculate sample kurtosis" << endl;
	return 1;
      }
      for (uint ii=0; ii<data.NumVoxels();++ii) {
	mm = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  mm += data(jj,ii);
	}
	mm /= (double)(data.NumTimePnts()-skip);
	results(volindex,ii) = 0.0;
	ss = 0.0;
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  results(volindex,ii) += (data(jj,ii)-mm)*(data(jj,ii)-mm)
	    * (data(jj,ii)-mm)*(data(jj,ii)-mm);
	  ss += (data(jj,ii)-mm)*(data(jj,ii)-mm);
	}
	ss = ss / nn;
	results(volindex,ii) = ((nn-1)/((nn-2)*(nn-3)))*((nn+1) *
				(results(volindex,ii)/nn)/(ss*ss)-3*(nn-1));
      }      
      ++volindex;
    }
    if (parg.Exists("min")) {
      for (uint ii=0; ii<data.NumVoxels();++ii) {
	results(volindex,ii) = data(skip,ii);
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  if (data(jj,ii) < results(volindex,ii)) {
	    results(volindex,ii) = data(jj,ii);
	  }
	}
      }
      ++volindex;
    }
    if (parg.Exists("max")) {
      for (uint ii=0; ii<data.NumVoxels();++ii) {
	results(volindex,ii) = data(skip,ii);
	for (uint jj=skip; jj<data.NumTimePnts(); ++jj) {
	  if (data(jj,ii) > results(volindex,ii)) {
	    results(volindex,ii) = data(jj,ii);
	  }
	}
      }
      ++volindex;
    }



    
    parg.Get("out",outfile);
    if (parg.Found()) {
      NIFTIFile nout;
      if (!nout.Open(outfile,'w')) {
	cerr << "Error opening output file" << endl;
	return 1;
      }
      if (!nout.Write(results,niftidatatype)) {
        cerr << "Error writing to output file" << endl;
        return 1;
      }
      
      
    }
  }

  return 0;
}
