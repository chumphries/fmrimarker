/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file fmrimconvert.cpp
  \author Colin Humphries
  \brief FMRIMConvert main function.
*/
/*!
  \mainpage FMRI Motion Artifact Removal using KErnel Regression

  \section intro_sec Introduction

  This is an overview.

 */

#include <iostream>
#include <fstream>
#include <string>
#include "argparser.h"
#include "mrivol.h"
#include "niftifile.h"
#include "marker.h"
// #include "envdefaults.h"
#include "mrirotate.h"
#include "version.h"



void print_help() {
  using namespace std;
  using namespace argparser;
  HelpText ht;
  // ostringstream ss(ios::ate);
  ht.SetFunctionName("fmrimconvert");
  ht.SetFunctionDescription("Convert between rotation matrix file formats");
  ht.SetFunctionVersion(MARKER_VERSION_STRING);
  ht.AddArgument("in","Input rotation matrix file(s)",-1);
  ht.AddArgument("out","Output rotation matrix file",1);
  ht.AddArgument("ipkg","Input software package (afni,fsl)",1);
  // ht.AddArgument("ispace","Input coordinate space (nifti,dicom,fsl)",1);
  // ht.AddArgument("iform","File format (nifti,dicom,fsl)",1);
  ht.AddArgument("ifslref","Input reference volume (FSL only)",1);
  ht.AddArgument("ifslorig","Input original volume (FSL only)",1);
  ht.AddArgument("opkg","Ouput software package (afni,fsl)",1);
  // ht.AddArgument("ospace","Output coordinate space (nifti,dicom,fsl)",1);
  ht.AddArgument("ofslref","Output reference volume (FSL only)",1);
  ht.AddArgument("ofslorig","Output original volume (FSL only)",1);
  ht.AddArgument("o4","Output rotation matrix in 4x4 format",0);
  ht.AddArgument("o12","Output rotation matrix as a 1x12 vector (default)",0);
  ht.AddArgument("inv","Invert input rotation matrix",0);

  cout << ht.ShortText() << endl;

}

/*!
  \fn main
  \brief Main function

  This is the main function for fmrimconvert
 */
int main (int argc, const char **argv) {
  using namespace std;
  using namespace argparser;
  using namespace fmri;
  
  // Parse command line options /////////////////////////////////////
  if (argc < 2) {
    print_help();
    return 0;
  }
  ArgParser parg(argv,argc);
  if (parg.Exists("help") || parg.Exists("h")) {
    print_help();
    return 0;
  }

  bool minvert = false;
  RotationSpace rspace = RotationSpace::NIFTI;
  RotationSpace orspace = RotationSpace::NIFTI;
  string matfile, outfile, ipkg, opkg, ispace, ospace;
  vector<string> matfiles;
  bool verbose = false, debug = false;

  if (debug) {
    verbose = true;
  }

  if (parg.Exists("verbose")) {
    verbose = true;
  }

  parg.Get("in",matfiles);
  if (!parg.Found() || parg.Error()) {
    cerr << "Error: No input file specified" << endl;
    return 1;
  }
  parg.Get("out",outfile);
  if (!parg.Found() || parg.Error()) {
    cerr << "Error: No output file specified" << endl;
    return 1;
  }

  MRIRotate mrot;
  vector<OrientationMatrix> matdata;

  parg.Get("ipkg",ipkg);
  if (parg.Found() && verbose) {
    cout << "Input matrix type: " << ipkg << endl;
  }
  /*
  parg.Get("ispace",ispace);
  if (parg.Found() && verbose) {
    cout << "Input coordinate space: " << ispace << endl;
  }
  */
  parg.Get("opkg",opkg);
  if (parg.Found() && verbose) {
    cout << "Output matrix type: " << opkg << endl;
  }
  /*
  parg.Get("ospace",ospace);
  if (parg.Found() && verbose) {
    cout << "Output coordinate space: " << ospace << endl;
  }
  */
  // Note that afni matrices are target -> source, while every
  // other package is source -> target. The next piece of code
  // figures out if we need to invert the matrix or not depending
  // on if we are using afni.
  if (ipkg.compare("afni") == 0) {
    rspace = RotationSpace::DICOM;
    if (opkg.compare("afni") == 0) {
      if (parg.Exists("inv")) {
	minvert = true;
      }
      else {
	minvert = false;
      }
    }
    else {
      if (parg.Exists("inv")) {
	minvert = false;
      }
      else {
	minvert = true;
      }
    }
  }
  else {
    if ((opkg.compare("afni") == 0) || parg.Exists("inv")) {
      // XOR
      minvert = true;
    }
  }
  if (ipkg.compare("fsl") == 0) {
    rspace = RotationSpace::FSL;
  }

  // cout << "Invert " << minvert << endl;
  mrot.ApplyInverse(minvert);
  if (verbose & minvert) {
    cout << "Inverting matrix" << endl;
  }
  /*
  if (ispace.compare("dicom") == 0) {
    rspace = RotationSpace::DICOM;
  }
  else if (ispace.compare("nifti") == 0) {
    rspace = RotationSpace::NIFTI;
  }
  else if (ispace.compare("fsl") == 0) {
    rspace = RotationSpace::FSL;
  }
  */
  mrot.SetInSpace(rspace);
  if (rspace == RotationSpace::FSL) {
    if (verbose) {
      cout << "Loading FSL target and source volumes for input matrix" << endl;
    }
    string targfile, sourcefile;
    NIFTIFile fin;
    parg.Get("ifslref",targfile);
    if (!parg.Found()) {
      cerr << "Error: input fsl reference file not defined." << endl;
      return 1;
    }
    parg.Get("ifslsrc",sourcefile);
    if (!parg.Found()) {
      cerr << "Error: input fsl orig file not defined." << endl;
      return 1;
    }
    if (!fin.Open(targfile,'r')) {
      cerr << "Error opening input reference file" << endl;
      return 1;
    }
    MRIBase target;
    fin.Read(target);
    fin.Close();
    MRIBase source;
    if (!fin.Open(sourcefile,'r')) {
      cerr << "Error opening input original file" << endl;
      return 1;
    }
    fin.Read(source);
    fin.Close();
    mrot.SetInSpaceFSL(source,target);
  }
  
  mrot.ReadMAT(matfiles,matdata);

  if (opkg.compare("fsl") == 0) {
    orspace = RotationSpace::FSL;
  }
  else if (opkg.compare("afni") == 0) {
    orspace = RotationSpace::DICOM;
  }
  /*
  if (ospace.compare("dicom") == 0) {
    orspace = RotationSpace::DICOM;
  }
  else if (ospace.compare("nifti") == 0) {
    orspace = RotationSpace::NIFTI;
  }
  else if (ospace.compare("fsl") == 0) {
    orspace = RotationSpace::FSL;
  }
  */
  mrot.SetOutSpace(orspace);
  if (orspace == RotationSpace::FSL) {
    if (verbose) {
      cout << "Loading FSL target and source volumes for output matrix" << endl;
    }
    string targfile, sourcefile;
    NIFTIFile fin;
    parg.Get("ofslref",targfile);
    if (!parg.Found()) {
      cerr << "Error: output ref file not defined." << endl;
      return 1;
    }
    parg.Get("ofslorig",sourcefile);
    if (!parg.Found()) {
      cerr << "Error: output orig file not defined." << endl;
      return 1;
    }
    if (!fin.Open(targfile,'r')) {
      cerr << "Error opening output ref file" << endl;
      return 1;
    }
    MRIBase target;
    fin.Read(target);
    fin.Close();
    MRIBase source;
    if (!fin.Open(sourcefile,'r')) {
      cerr << "Error opening output orig file" << endl;
      return 1;
    }
    fin.Read(source);
    fin.Close();
    mrot.SetOutSpaceFSL(source,target);
  }

  if (parg.Exists("o4")) {
    mrot.WriteMAT4(outfile,matdata);
  }
  else {
    mrot.WriteMAT(outfile,matdata);
  }


  return 0;
}
