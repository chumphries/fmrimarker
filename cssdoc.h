/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of XMLGen.
*
* XMLGen is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* XMLGen is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with XMLGen.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file cssdoc.h
  \author Colin Humphries
  \brief Classes for creating CSS elements.
*/
#ifndef _CSSDOC_H
#define _CSSDOC_H

#include "xmldoc.h"

namespace xmlgen {
  
  /*! \class CSSStyleTag
    \brief CSS Style Tag
    
    This class is designed to store and render CSS style
    definitions. The class works like a general XTag object with the
    tagname: 'style'. In addition, a function called 'addDeclaration' is
    defined, which let's one add multiple CSS style definitions. The
    definitions are stored in a private dictionary-type variable using
    the map template. The common definitions are grouped together and
    combined into single statements during rendering.
    
    For example:
     addDeclaration("div","border","1px solid black");
     addDeclaration("div","margin","auto");
     addDeclaration("p","text-align","right");
    would result in:
     <style>
      div {border: 1px solid black;margin: auto;}
      p {text-align: right;}
     </style>
    \author Colin Humphries
  */
  class CSSStyleTag : public XTag {
  public:
    CSSStyleTag() {
      name = "style";
      addAttribute("type","text/css");
      pstylestring = addString("");
    }
    /*!
      Add a new CSS style definition.
      \param[in] select CSS class (e.g., "p", ".classname", "#idname table,tr,td", ...)
      \param[in] property CSS property (e.g., "margin")
      \param[in] value CSS property value (e.g., "1px")
    */ 
    void addDeclaration(const std::string &select, const std::string &property,
			const std::string &value) {
      std::string dec = property + ":" + value + ";";
      styles[select].push_back(dec);
      ismodified = true;
    }
    /*!
      Output string rendering of tag and children.
    */
    std::string Render() {
      // Note that because properties are grouped together based on class and new
      // properties can be added at any time, we wait until Render() is called to
      // generate the render string.
      // Also, we use the ismodified variable to keep track of whether a new
      // declaration has been made, so that we only regenerate the string as needed.
      if (ismodified) {
	std::ostringstream ss;
	for (std::map<std::string, std::vector<std::string> >::iterator it = styles.begin();
	     it != styles.end(); ++it) {
	  if (it != styles.begin()) {
	    ss << std::endl;
	  }
	  ss << it->first << " {";
	  for (std::vector<std::string>::iterator it2 = (it->second).begin();
	       it2 != (it->second).end(); ++it2) {
	    ss << *it2;
	  }
	  ss << "}";
	}
	pstylestring->setString(ss.str());
      }
      return XTag::Render();
    }
    
  private:
    std::map<std::string, std::vector<std::string> > styles;
    XString *pstylestring;
  };
  
}

#endif
  
