/*********************************************************************
* Copyright 2018, Colin Humphries
*
* This file is part of ArgParser.
*
* ArgParser is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ArgParser is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ArgParser.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file argparser.h
  \author Colin Humphries
  \brief Argument parsing classes.
 */
#ifndef _ARGPARSER_H
#define _ARGPARSER_H

#include <string>
#include <vector>
#include <tuple>
#include <sstream>
#include <locale>
#include <map>
#include <stdexcept>
#include <algorithm>
#include <cctype>

// using namespace std;

namespace argparser {
  
  struct ArgIndex {
    size_t start;
    size_t end;
  };
  
  class ArgEntry {
  public:
    ArgEntry() {;}
    int start;
    int end;
    std::string label;
  };
  
  /*! \class ArgParser
    \brief An argument parsing class.
    
    This class is used to parse command line arguments.
    
    \author Colin Humphries
  */
  class ArgParser {
  public:
    /*!
      Class constructor.
      \param argv pointer to list of command line strings.
      \param argc number of command line strings.
    */
    ArgParser(const char **argv, int argc) {
      ResetError();
      Parse(argv,argc);
    }
    /*!
      Copy strings to internal variable args.
      \param argv pointer to list of command line strings.
      \param argc number of command line strings.
    */
    void Parse(const char **argv, int ac) {
      args.clear();
      argmap.clear();
      arglist.clear();
      uint argc = (uint)ac;
      std::string astr, lastname;
      ArgIndex aind;
      ArgEntry aent;
      
      aind.start = -1;
      aent.start = -1;
      for (uint ii=1; ii<argc; ++ii) {
	astr = argv[ii];
	args.push_back(astr);
	if (astr.size() < 2) {
	  continue;
	}
	if (astr[0] == '-' && !(isdigit(astr[1]) || astr[1] == '.')) {
	  if (aent.start >= 0) {
	    aind.end = ii-2;
	    aent.end = ii-2;
	    argmap[lastname] = aind;
	    aent.label = lastname;
	    arglist.push_back(aent);
	    // cout << lastname << endl;
	  }
	  lastname = astr.substr(1,std::string::npos);
	  aind.start = ii-1;
	  aent.start = ii-1;
	}
      }
      if (aent.start >= 0) {
	aind.end = argc-2;
	aent.end = argc-2;
	argmap[lastname] = aind;
	aent.label = lastname;
	arglist.push_back(aent);
      }
    }
    /*!
      Tests if a specific argument was included.
      \param argname name of the argument
      \return true/false
    */
    bool Exists(const std::string &argname) {
      ResetError();
      return FindArg(argname);
    }
    size_t NumArgs(const std::string &argname) {
      size_t acount = 0;
      for (std::vector<ArgEntry>::iterator it=arglist.begin();
	   it != arglist.end(); ++it) {
	if (argname.compare(it->label) == 0) {
	  ++acount;
	}
      }
      return acount;
    }
    /*!
      Return a list of all the argument labels.
      \param[out] vector of strings
    */
    void ArgLabels(std::vector<std::string> &al) {
      al.clear();
      for (std::vector<ArgEntry>::iterator it=arglist.begin();
	   it != arglist.end(); ++it) {
	al.push_back(it->label);
      }
    }
    // Getting strings ////////////////////////
    /*!
      Get argument values returned as a vector of strings.
      \param[in] ta argument name
      \param[out] val vector of argument values
    */
    void Get(const std::string &ta, std::vector<std::string> &val) {
      Get(ta,-1,val);
    }
    /*!
      Get a specific number of argument values returned as a vector of strings.
      \param[in] ta argument name
      \param[num] num number of values
      \param[out] val vector of argument values
    */
    void Get(const std::string &ta, int inum, std::vector<std::string> &val) {
      val.clear();
      ResetError();
      size_t start, end, num;
      if (!FindArg(ta,start,end)) {
	return;
      }
      if (inum < 0) {
	num = end-start;
      }
      else {
	num = inum;
      }
      if (num > (end-start)) {
	iserror = true;
	notenoughvalues = true;
	num = end-start;
      }
      
      for (uint ii=start+1; ii<=(start+num); ++ii) {
	val.push_back(args[ii]);
      }
    }
    /*!
      Get a specific argument value returned as a string.
      \param[in] ta argument name
      \param[num] ind index of argument value
      \param[out] val argument value
    */
    void Get(const std::string &ta, size_t ind, std::string &val) {
      ResetError();
      size_t start, end;
      if (!FindArg(ta,start,end)) {
	return;
      }
      if (ind >= (end-start)) {
	iserror = true;
	return;
      }
      val = args[start+ind+1];
    }
    /*!
      Get the first argument value returned as a string.
      \param[in] ta argument name
      \param[out] val argument value
    */
    void Get(const std::string &ta, std::string &val) {
      Get(ta,0,val);
    }
    // Getting integers
    /*!
      Get argument values returned as a vector of signed integers.
      \param[in] ta argument name
      \param[out] val vector of argument values
    */  
    void Get(const std::string &ta, std::vector<int> &val) {
      Get(ta,-1,val);
    }
    /*!
      Get a specific number of argument values returned as a vector of signed integers.
      \param[in] ta argument name
      \param[num] num number of values
      \param[out] val vector of argument values
    */  
    void Get(const std::string &ta, int num, std::vector<int> &val) {
      val.clear();
      std::vector<std::string> tstr;
      Get(ta,num,tstr);
      if (!found || (iserror && !notenoughvalues)) { // Fix this later...
	return;
      }
      for (std::vector<std::string>::iterator it = tstr.begin();
	   it != tstr.end(); ++it) {
	int ival;
	try {
	  ival = std::stol(*it);
	}
	catch (const std::invalid_argument &ia) {
	  iserror = true;
	  casterror = true;
	  return;
	}
	val.push_back(ival);
      }
    }
    /*!
      Get a specific argument value returned as a signed integer.
      \param[in] ta argument name
      \param[num] ind index of argument value
      \param[out] val argument value
    */
    void Get(const std::string &ta, size_t ind, int &val) {
      std::string tstr;
      Get(ta,ind,tstr);
      if (!found || iserror) {
	return;
      }
      try {
	val = stol(tstr);
      }
      catch (const std::invalid_argument &ia) {
	iserror = true;
	casterror = true;
	return;
      }
    }
    /*!
      Get the first argument value returned as a signed integer.
      \param[in] ta argument name
      \param[out] val argument value
    */
    void Get(const std::string &ta, int &val) {
      Get(ta,0,val);
    }
    // Getting unsigned integers
    /*!
      Get argument values returned as a vector of unsigned integers.
      \param[in] ta argument name
      \param[out] val vector of argument values
    */    
    void Get(const std::string &ta, std::vector<uint> &val) {
      Get(ta,-1,val);
    }
    /*!
      Get a specific number of argument values returned as a vector of unsigned integers.
      \param[in] ta argument name
      \param[num] num number of values
      \param[out] val vector of argument values
    */  
    void Get(const std::string &ta, int num, std::vector<uint> &val) {
      val.clear();
      std::vector<int> tval;
      Get(ta,num,tval);
      if (!found || iserror) {
	return;
      }
      for (std::vector<int>::iterator it = tval.begin(); it != tval.end(); ++it) {
	if (*it >= 0) {
	  val.push_back(*it);
	}
	else {
	  iserror = true;
	  return;
	}
      }
    }
    /*!
      Get a specific argument value returned as an unsigned integer.
      \param[in] ta argument name
      \param[num] ind index of argument value
      \param[out] val argument value
    */
    void Get(const std::string &ta, size_t ind, uint &val) {
      int tval;
      Get(ta,ind,tval);
      if (!found || iserror) {
	return;
      }
      if (tval >= 0) {
	val = tval;
      }
      else {
	iserror = true;
      }
    }
    /*!
      Get the first argument value returned as an unsigned integer.
      \param[in] ta argument name
      \param[out] val argument value
    */
    void Get(const std::string &ta, uint &val) {
      Get(ta,0,val);
    }
    // Getting doubles
    /*!
      Get argument values returned as a vector of doubles.
      \param[in] ta argument name
      \param[out] val vector of argument values
    */
    void Get(const std::string &ta, std::vector<double> &val) {
      Get(ta,-1,val);
    }
    /*!
      Get a specific number of argument values returned as a vector of doubles.
      \param[in] ta argument name
      \param[num] num number of values
      \param[out] val vector of argument values
    */  
    void Get(const std::string &ta, int num, std::vector<double> &val) {
      val.clear();
      std::vector<std::string> tstr;
      Get(ta,num,tstr);
      if (!found || (iserror && !notenoughvalues)) {
	return;
      }
      for (std::vector<std::string>::iterator it = tstr.begin();
	   it != tstr.end(); ++it) {
	double ival;
	try {
	  ival = stod(*it);
	}
	catch (const std::invalid_argument &ia) {
	  iserror = true;
	  casterror = true;
	  return;
	}
	val.push_back(ival);
      }
    }
    /*!
      Get a specific argument value returned as a double.
      \param[in] ta argument name
      \param[num] ind index of argument value
      \param[out] val argument value
    */
    void Get(const std::string &ta, size_t ind, double &val) {
      std::string tstr;
      Get(ta,ind,tstr);
      if (!found || iserror) {
	return;
      }
      try {
	val = std::stod(tstr);
      }
      catch (const std::invalid_argument &ia) {
	iserror = true;
	casterror = true;
	return;
      }
    }
    /*!
      Get the first argument value returned as a double.
      \param[in] ta argument name
      \param[out] val argument value
    */
    void Get(const std::string &ta, double &val) {
      Get(ta,0,val);
    }
    // Getting bool
    /*!
      Get the boolean value of an argument.
      \param[in] ta argument name
      \param[out] val argument value (ie yes,no,1,0,on,off,true,false)
    */  
    void Get(const std::string &ta, bool &val) {
      std::string tstr;
      Get(ta,tstr);
      if (!found || iserror) {
	return;
      }
      transform(tstr.begin(),tstr.end(),
		tstr.begin(), ::tolower);
      if (tstr.compare("yes") == 0) {
	val = true;
      }
      else if (tstr.compare("no") == 0) {
	val = false;
      }
      else if (tstr.compare("1") == 0) {
	val = true;
      }
      else if (tstr.compare("0") == 0) {
	val = false;
      }
      else if (tstr.compare("on") == 0) {
	val = true;
      }
      else if (tstr.compare("off") == 0) {
	val = false;
      }
      else if (tstr.compare("true") == 0) {
	val = true;
      }
      else if (tstr.compare("false") == 0) {
      val = false;
      }
      else {
	iserror = true;
      }
    }
    /*!
      Number of arguments in class
      \return number of arguments
    */
    size_t Size() const {return args.size();}
    /*!
      Check for parsing error during the last Get() call
      \return true if there was an error
    */
    bool Error() const {return iserror;}
    // bool NotFound() {return notfound;} // Note: a notfound is not an error.
    /*!
      Check if the specified argument was found during the last Get() call
      \return true if argument was found
    */
    bool Found() const {return found;}
    /*!
      Check if there was a casting error during the last Get() call
      \return true if casting error occurred
    */
    bool CastError() const {return casterror;}
    /*!
      Check if the user did not specify enough values during the last Get() call
      \return true if there were not enough values
    */  
    bool NotEnoughValues() const {return notenoughvalues;}
    // bool TooManyValues() {return toomanyvalues;}
  private:
    /*!
      Reset error flags
    */  
    void ResetError() {
      iserror = false;
      // notfound = false;
      found = false;
      casterror = false;
      notenoughvalues = false;
      // toomanyvalues = false;
    }
    bool FindArg(const std::string &ta) {
      size_t s, e;
      return FindArg(ta,s,e);
    }
    /*!
      Search for specific argument
      \param[in] ta argument name
      \param[out] start index of first argument value
      \param[out] end index of last argument value
      \return true if argument was found
    */
    bool FindArg(const std::string &ta, size_t &start, size_t &end) {
      if (ta.size() < 1) {
	return false;
      }
      std::string alb = ta;
      int aindex = 0;
      if (ta[ta.size()-1] == ']') {
	int ii;
	for (ii=ta.size()-2; ii>=0; --ii) {
	  if (ta[ii] == '[') {
	    break;
	  }
	}
	if (ii <= 1 || ii == (int)ta.size()-2) {
	  return false;
	}
	try {
	  aindex = std::stol(ta.substr(ii+1,ta.size()-2-ii));
	}
	catch (const std::invalid_argument &ia) {
	  return false;
	}
	if (aindex < 0) {
	  return false;
	}
	alb = ta.substr(0,ii);
      }
      int acount = 0;
      for (std::vector<ArgEntry>::iterator it=arglist.begin();
	   it != arglist.end(); ++it) {
	if (alb.compare(it->label) == 0) {
	  if (acount < aindex) {
	    ++acount;
	    continue;
	  }
	  start = it->start;
	  end = it->end;
	  found = true;
	  return true;    
	}
      }
      return false;
    }

    std::vector<std::string> args;
    std::map<std::string,ArgIndex> argmap;
    std::vector<ArgEntry> arglist;
    
    bool iserror;
    // bool notfound;
    bool found;
    bool casterror;
    bool notenoughvalues;
    // bool toomanyvalues;
    
    
  };
  
  
  // ////////////////////////////////////
  
  struct ArgHelp {
    std::string name;
    std::string descr_short;
    std::string descr_long;
    int numarg;
    bool required;
  };
  
  struct ArgDescription {
    std::vector<std::string> args;
    std::string text;
  };
  
  /*! \class HelpText
    \brief A class for generating help messages.
    
    This class is used to
    
    \author Colin Humphries
  */
  class HelpText {
  public:
    /*!
      Class constructor.
    */
    HelpText() {
      usage = true;
      numcol = 100;
    }
    /*!
      Generate help message.
      \return help message
    */
    std::string Text() {
      std::ostringstream ss;
      if (function.size()) {
	ss << prestring;
	if (usage) {
	  ss << "usage: ";
	}
	ss << function;
	if (arguments.size()) {
	  ss << " [options]";
	}
	ss << std::endl;
	if (func_description.size()) {
	  ss << prestring << " " << func_description << std::endl;
	}
      }
      int argwidth1 = 0, argwidth2 = 0, ws;
      if (arguments.size()) {
	ss << prestring << " options:" << std::endl;
	MaxArgWidth(argwidth1,argwidth2);
	// cout << argwidth1 << " " << argwidth2 << endl;
      }
      for (std::vector<ArgHelp>::const_iterator it = arguments.begin();
	   it != arguments.end(); ++it) {
	ss << prestring << "  -" << it->name;
	ws = (it->name).size();
	for (int ii=ws; ii<argwidth1+1; ++ii) {
	  ss << " ";
	}
	std::string svl = ArgValList(it->numarg);
	ss << svl;
	ws = svl.size();
	for (int ii=ws; ii<argwidth2+1; ++ii) {
	  ss << " ";
	}
	if ((int)(it->descr_short).size() > numcol - (argwidth1+argwidth2+5)) {
	  std::string fline(it->descr_short);
	  WordWrap(fline,numcol-(argwidth1+argwidth2+5));
	  std::istringstream iss(fline);
	  std::string line;
	  bool sss = false;
	  while(getline(iss,line)) {
	    if (sss) {
	      for (int ii=0; ii<(argwidth1+argwidth2+5); ++ii) {
		ss << " ";
	      }
	    }
	    else {
	      sss = true;
	    }
	    ss << line << std::endl;
	  }
	}
	else {
	  ss << it->descr_short << std::endl;
	}
      }
      return ss.str();
    }
    std::string ShortText() {
      std::ostringstream ss;
      ss << Text();
      if (func_version.size()) {
	ss << " version: " << func_version;
      }
      return ss.str();
    }
    std::string LongText() {
      std::ostringstream ss;
      ss << Text();
      ss << " description:" << std::endl;
      for (std::vector<ArgDescription>::const_iterator it = descriptions.begin();
	   it != descriptions.end(); ++it) {
	if ((it->args).size() > 0) {
	  for (std::vector<std::string>::const_iterator it2 = (it->args).begin();
	       it2 != (it->args).end(); ++it2) {
	    ss << "  " << *it2 << std::endl;
	  }
	}
	std::istringstream iss(it->text);
	std::string line;
	while(getline(iss,line)) {
	  ss << "    " << line << std::endl;
	}
	
      }
      if (func_version.size()) {
	ss << " version: " << func_version;
      }
      return ss.str();
    }
    
    /*!
      Set the name of the program.
      \param fn function name
    */
    void SetFunctionName(const std::string &fn) {function = fn;}
    /*!
      Set the description of the program.
      \param fd function description
    */
    void SetFunctionDescription(const std::string &fd) {func_description = fd;}
    /*!
      Set the verion of the program.
      \param ver version string
    */
    void SetFunctionVersion(const std::string &ver) {func_version = ver;}
    /*!
      Add a command line argument.
      \param argname argument name
      \param argdescription argument description
      \param pn number of parameters (-1 = variable number)
    */
    void AddArgument(const std::string &aname, 
		     const std::string &desc, int pn) {
      ArgHelp ah;
      ah.name = aname;
      ah.descr_short = desc;
      ah.numarg = pn;
      ah.required = false;
      arguments.push_back(ah);
    }
    void AddArgument(const std::string &aname, 
		     const std::string &desc, int pn,
		     bool req) {
      ArgHelp ah;
      ah.name = aname;
      ah.descr_short = desc;
      ah.numarg = pn;
      ah.required = req;
      arguments.push_back(ah);
    }  
    void AddArgument(const std::string &aname, 
		     const std::string &desc, int pn,
		     const std::string &ldesc) {
      ArgHelp ah;
      ah.name = aname;
      ah.descr_short = desc;
      ah.numarg = pn;
      ah.required = false;
      ah.descr_long = ldesc;
      // WordWrap(ah.descr_long);
      arguments.push_back(ah);
    }
    void AddArgument(const std::string &aname, 
		     const std::string &desc, int pn,
		     bool req, const std::string &ldesc) {
      ArgHelp ah;
      ah.name = aname;
      ah.descr_short = desc;
      ah.numarg = pn;
      ah.required = req;
      ah.descr_long = ldesc;
      arguments.push_back(ah);
    }
    void AddDescription(const std::string &dtext) {
      ArgDescription ad;
      ad.text = dtext;
      WordWrap(ad.text,numcol-4);
      descriptions.push_back(ad);
    }
    void AddDescription(const std::string &darg, const std::string &dtext) {
      ArgDescription ad;
      (ad.args).push_back(darg);
      ad.text = dtext;
      WordWrap(ad.text,numcol-4);
      descriptions.push_back(ad);
    }
    void AddDescription(const std::vector<std::string> &darg, const std::string &dtext) {
      ArgDescription ad;
      ad.args = darg;
      ad.text = dtext;
      WordWrap(ad.text,numcol-4);
      descriptions.push_back(ad);
    }
    void SetUsage(bool su) {usage = su;}
    void SetPreString(const std::string &ps) {prestring = ps;}
    /*!
      Clear class data
    */
    void Clear() {
      function.clear();
      func_description.clear();
      arguments.clear();
    }
    void WordWrap(std::string &ostr, int ncol) {
      
      std::string::iterator it1 = ostr.end();
      int count = 1;
      for (std::string::iterator it2 = ostr.begin(); it2 != ostr.end(); ++it2) {
	if (*it2 == '\n') {
	  it1 = ostr.end();
	  count = 0;
	}
	else if (isspace(*it2)) {
	  it1 = it2;
	}
	if (count >= ncol) {
	  if (it1 != ostr.end()) {
	    *it1 = '\n';
	    it2 = it1;
	    count = 0;
	  }
	}
	++count;
      }
    }
  private:
    std::string ArgValList(int pn) {
      std::string al;
      if (pn < 0) {
	al = "<1> ...";
      }
      else if (pn == 1) {
	al = "<1>";
      }
      else if (pn > 1) {
	for (int ii=0; ii<pn; ++ii) {
	  if (ii == 0) {
	    al += "<";
	  }
	  else {
	    al += " <";
	  }
	  al += std::to_string(ii+1);
	  al += ">";	
	}
      }
      return al;
    }
    void MaxArgWidth(int &mwidth1, int &mwidth2) {
      int ws;
      std::string aex;
      mwidth1 = 0;
      mwidth2 = 0;
      for (std::vector<ArgHelp>::const_iterator it = arguments.begin();
	   it != arguments.end(); ++it) {
	ws = (it->name).size();
	if (ws > mwidth1) {
	  mwidth1 = ws;
	}
	aex = ArgValList(it->numarg);
	ws = aex.size();
	if (ws > mwidth2) {
	  mwidth2 = ws;
	}
      }
    }
    std::string function;
    std::string func_description;
    std::string func_version;
    std::vector<ArgHelp> arguments;
    bool usage;
    std::string prestring;
    int numcol;
    std::vector<ArgDescription> descriptions;
  };

}

#endif
