FMRIMarker
==========

FMRIMarker is a tool for removing motion artifact from fMRI data using kernel regression.

Features
--------

- Supports AFNI and FSL

Installation
------------

CMake is necessary to generate the Makefiles for compiling the project. On Linux, cmake can be installed using the distribution's package manager. On MacOS, cmake can be installed using a third-party package manager such as Homebrew.

On the command line, type:

    cmake .

followed by:

    make

Note on MacOS compilation: Fmrimarker uses the OpenMP library for multi-threaded processing. Currently, the clang compiler distributed with MacOS does not support OpenMP. Fmrimarker will compile with clang, but it will only run using a single thread. To compile with OpenMP, install gcc using a package manager like Homebrew. Then explicitly specify the gcc compiler when calling cmake.

    CC=gcc CXX=g++ cmake .

On some systems the gcc command is linked to clang, so it might be necessary to specify the compiler version (e.g., version 12 on my machine)

    CC=gcc-12 CXX=g++-12 cmake .

Usage
-----

Please read the included user manual for detailed instructions on running fmrimarker.

Support
-------

Please email me if you are having problems or have any questions about using FMRIMarker.
Colin Humphries
chumphri@mcw.edu

License
-------

FMRIMarker is licensed under the GNU General Public License, version 3 or any later version.
