/*********************************************************************
* Copyright 2016, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file svgbargraph.h
  \author Colin Humphries
  \brief Classes for creating SVG documents.
*/
#ifndef _SVGBARGRAPH_H
#define _SVGBARGRAPH_H

#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <cmath>
#include <iomanip>

#include "svggraph.h"

using namespace std;

class SVGBarGraph : public SVGGraph {
public:
  SVGBarGraph() {

  }

  virtual string Render() {
    return SVGGraph::Render();
  }

  void Plot() {
    // PlotAxis();
    SVGGraph::Plot();
    XTag *pxt;
    for (vector<LineData>::iterator it = data.begin(); it != data.end(); ++it) {
      vector<double> newx = it->xdata;
      vector<double> newy = it->ydata;
      ConvertPnts(newx,newy);
      
      double qq1, qq2;
      for (uint ii = 0; ii < newx.size(); ++ii) {
	if (ii == 0) {
	  qq1 = newx[ii]-((newx[ii+1]-newx[ii])/2.0);
	  qq2 = newx[ii] + ((newx[ii+1]-newx[ii])/2.0);
	}
	else if (ii == (newx.size()-1)) {
	  qq1 = newx[ii-1] + ((newx[ii]-newx[ii-1])/2.0);
	  qq2 = newx[ii] + ((newx[ii]-newx[ii-1])/2.0);
	}
	else {
	  qq1 = newx[ii-1] + ((newx[ii]-newx[ii-1])/2.0);
	  qq2 = newx[ii] + ((newx[ii+1]-newx[ii])/2.0);
	  
	}
	pxt = addRect(qq1,newy[ii],qq2-qq1,graphy+graphh-newy[ii]);
	pxt->addAttribute("style","stroke:" + it->linecolor + 
			  ";fill:" + it->markercolor + ";");
      }      
    }

  }

};

#endif
