/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file mrirotate.h
  \author Colin Humphries
  \brief Class for loading default settings.
*/
#ifndef _MRIROTATE_H
#define _MRIROTATE_H
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <cmath>
#include <limits>

#include "mrivol.h"

// using namespace std;

enum class RotationType {AFNI,FSL,NIFTI};
enum class RotationSpace {NIFTI,DICOM,FSL};

struct RParameters {
  double xtrans;
  double ytrans;
  double ztrans;
  double roll;
  double pitch;
  double yaw;
};

class MRIRotate {
public:
  MRIRotate() {
    rtype = RotationType::AFNI;
    inspace = RotationSpace::NIFTI;
    outspace = RotationSpace::NIFTI;
    applyinv = false;
    FOCThresh = 0.2153 * 0.2153;
  }
  void ReadAFNI(const std::string &filename,
		std::vector<fmri::OrientationMatrix> &omat) {
    SetInSpaceDICOM();
    ReadMAT(filename,omat);
  }
  void ReadMAT(const std::vector<std::string> &filenames,
	       std::vector<fmri::OrientationMatrix> &omat) {
    for (std::vector<std::string>::const_iterator it=filenames.begin();
	 it!=filenames.end(); ++it) {
      ReadMAT(*it,omat);
    }
  }
  void ReadMAT(const std::string &filename, std::vector<fmri::OrientationMatrix> &omat) {
    // Can be either a Nx12, Nx16, or 4x4 file
    std::fstream fin;
    fin.open(filename,std::fstream::in);
    if (fin.fail()) {
      return;
    }
    std::string lstr;
    std::vector<double> vals;
    fmri::OrientationMatrix otmp;
    while (1) {
      getline(fin,lstr);
      if (fin.eof()) {
	break;
      }
      if (lstr.empty()) {
	continue;
      }
      if (lstr[0] == '#') {
	continue;
      }

      std::istringstream iss(lstr);
      vals.clear();
      copy(std::istream_iterator<double>(iss),std::istream_iterator<double>(),
	   std::back_inserter(vals));

      if (vals.size() == 4) {
	// Assume we are dealing with a single 4x4 matrix
	getline(fin,lstr);
	std::istringstream iss2(lstr);
	copy(std::istream_iterator<double>(iss2),std::istream_iterator<double>(),
	     std::back_inserter(vals));
	getline(fin,lstr);
	std::istringstream iss3(lstr);
	copy(std::istream_iterator<double>(iss3),std::istream_iterator<double>(),
	     std::back_inserter(vals));
	// Ignore last line
	MatCopy(otmp,vals);
	omat.push_back(otmp);
	break;
      }
      if (vals.size() != 12 && vals.size() != 16) {
	break;
      }
      MatCopy(otmp,vals);
      omat.push_back(otmp);
    }
    fin.close();
  }
  
  void ReadFSLDir(const std::string &dirname, std::vector<fmri::OrientationMatrix> &omat) {
    std::vector<std::string> filelist;
    int count = 0;
    std::fstream fin;
    while(1) {
      std::ostringstream ss;
      ss << dirname << "/" << "MAT_";
      ss << std::setw(4) << std::setfill('0') << count;
      fin.open(ss.str(),std::fstream::in);
      if (fin.fail()) {
	break;
      }
      fin.close();
      filelist.push_back(ss.str());
      ++count;
    }

    SetInSpaceFSL();
    ReadMAT(filelist,omat);
  }
  void ReadPAR(const std::string &filename, std::vector<RParameters> &omat) {
    std::fstream fin;
    fin.open(filename,std::fstream::in);
    if (fin.fail()) {
      return;
    }
    std::string lstr;
    std::vector<double> vals;
    RParameters rp;
    while (1) {
      getline(fin,lstr);
      if (fin.eof()) {
	break;
      }
      if (lstr.empty()) {
	continue;
      }
      if (lstr[0] == '#') {
	continue;
      }

      std::istringstream iss(lstr);
      vals.clear();
      copy(std::istream_iterator<double>(iss),std::istream_iterator<double>(),
	   std::back_inserter(vals));
      if (vals.size() == 6) {
	/*
	rp.xtrans = vals[0];
	rp.ytrans = vals[1];
	rp.ztrans = vals[2];
	rp.roll = vals[3];
	rp.pitch = vals[4];
	rp.yaw = vals[5];
	*/
	rp.xtrans = vals[4];
	rp.ytrans = vals[5];
	rp.ztrans = vals[3];
	rp.roll = vals[0];
	rp.pitch = vals[1];
	rp.yaw = vals[2];
	omat.push_back(rp);
      }
    }
    fin.close();
  }
  void ReadPARFSL(const std::string &filename, std::vector<RParameters> &omat) {
    std::fstream fin;
    fin.open(filename,std::fstream::in);
    if (fin.fail()) {
      return;
    }
    std::string lstr;
    std::vector<double> vals;
    RParameters rp;
    while (1) {
      getline(fin,lstr);
      if (fin.eof()) {
	break;
      }
      if (lstr.empty()) {
	continue;
      }
      if (lstr[0] == '#') {
	continue;
      }

      std::istringstream iss(lstr);
      vals.clear();
      copy(std::istream_iterator<double>(iss),std::istream_iterator<double>(),
	   std::back_inserter(vals));
      if (vals.size() == 6) {
	rp.xtrans = vals[4];
	rp.ytrans = vals[5];
	rp.ztrans = vals[6];
	rp.roll = vals[0] * (180.0/M_PI);
	rp.pitch = vals[1] * (180.0/M_PI);
	rp.yaw = vals[2] * (180.0/M_PI);
	omat.push_back(rp);
      }
    }
    fin.close();
  }
  double FOC(const std::vector<RParameters> &rp) {
    // Frame Orientation Count
    double foc = 0.0, dp, dist;
    // See comment in FD function below
    double DTOMMSQ = (100*M_PI/360) * (100*M_PI/360);
    for (std::vector<RParameters>::const_iterator it1 = rp.begin();
	 it1 != rp.end(); ++it1) {
      dp = 0.0;
      for (std::vector<RParameters>::const_iterator it2 = rp.begin();
	   it2 != rp.end(); ++it2) {
	if (it1 != it2) {
	  dist = DTOMMSQ*(it1->roll-it2->roll)*(it1->roll-it2->roll) +
            DTOMMSQ*(it1->pitch-it2->pitch)*(it1->pitch-it2->pitch) +
            DTOMMSQ*(it1->yaw-it2->yaw)*(it1->yaw-it2->yaw) +
            (it1->xtrans-it2->xtrans)*(it1->xtrans-it2->xtrans) +
            (it1->ytrans-it2->ytrans)*(it1->ytrans-it2->ytrans) +
            (it1->ztrans-it2->ztrans)*(it1->ztrans-it2->ztrans);
	}
	else {
	  dist = 0.0;
	}
	if (dist < FOCThresh) {
	  dp += 1.0;
	}
      }
      foc += (1.0 / dp);
    }
    return foc;
  }
  void FD(const std::vector<RParameters> &rp, std::vector<double> &data) {
    double fd;
    // In framewise displacement, the rotation angles are converted
    // to a distance by calculating the corresponding distance along
    // a 50 mmm radius circle. The diameter of this circle would be
    // 100*pi so to convert from degrees to mm we multiply by 100pi/360
    double DTOMM = 100.0*M_PI/360.0;
    data.push_back(0.0);
    for (uint ii=1; ii<rp.size(); ++ii) {
      fd = DTOMM*std::abs(rp[ii].roll-rp[ii-1].roll) + 
        DTOMM*std::abs(rp[ii].pitch-rp[ii-1].pitch) +
        DTOMM*std::abs(rp[ii].yaw-rp[ii-1].yaw) + 
	std::abs(rp[ii].xtrans-rp[ii-1].xtrans) +
        std::abs(rp[ii].ytrans-rp[ii-1].ytrans) + 
	std::abs(rp[ii].ztrans-rp[ii-1].ztrans);
      // std::cout << DTOMM*std::abs(rp[ii].roll-rp[ii-1].roll) << std::endl;
      data.push_back(fd);
    }
  }
  void FD(const std::vector<RParameters> &rp, double &dmean, double &dmax) {
    double fd;
    // In framewise displacement, the rotation angles are converted
    // to a distance by calculating the corresponding distance along
    // a 50 mmm radius circle. The diameter of this circle would be
    // 100*pi so to convert from degrees to mm we multiply by 100pi/360
    double DTOMM = 100*M_PI/360;
    dmean = 0.0;
    dmax = 0.0;
    for (uint ii=1; ii<rp.size(); ++ii) {
      fd = DTOMM*std::abs(rp[ii].roll-rp[ii-1].roll) + 
        DTOMM*std::abs(rp[ii].pitch-rp[ii-1].pitch) +
        DTOMM*std::abs(rp[ii].yaw-rp[ii-1].yaw) + 
	std::abs(rp[ii].xtrans-rp[ii-1].xtrans) +
        std::abs(rp[ii].ytrans-rp[ii-1].ytrans) + 
	std::abs(rp[ii].ztrans-rp[ii-1].ztrans);
      dmean += fd;
      if (fd > dmax) {
	dmax = fd;
      }
    }
    dmean /= (double)rp.size();
  }
  void MAT2PAR(const std::vector<fmri::OrientationMatrix> &mat,
	       std::vector<RParameters> &rp,
	       const fmri::MRIBase &base, const fmri::MRIBase &source) {
    double cthresh = std::numeric_limits<double>::epsilon();
    fmri::OrientationMatrix btrans, strans, mattmp;
    double xp, yp, zp, cy;
    xp = (double)(source.XSize()-1);
    yp = (double)(source.YSize()-1);
    zp = (double)(source.ZSize()-1);
    source.OrientMatrix().Transform(xp,yp,zp);
    // xp = -1.0 * (source.Orient(0,3) + xp) / 2.0;
    // yp = -1.0 * (source.Orient(1,3) + yp) / 2.0;
    xp = (source.Orient(0,3) + xp) / 2.0;
    yp = (source.Orient(1,3) + yp) / 2.0;
    zp = (source.Orient(2,3) + zp) / 2.0;
    strans(0,3) = xp;
    strans(1,3) = yp;
    strans(2,3) = zp;
    xp = (double)(base.XSize()-1);
    yp = (double)(base.YSize()-1);
    zp = (double)(base.ZSize()-1);
    base.OrientMatrix().Transform(xp,yp,zp);
    // xp = -1.0 * (base.Orient(0,3) + xp) / 2.0;
    // yp = -1.0 * (base.Orient(1,3) + yp) / 2.0;
    xp = (base.Orient(0,3) + xp) / 2.0;
    yp = (base.Orient(1,3) + yp) / 2.0;
    zp = (base.Orient(2,3) + zp) / 2.0;
    strans(0,3) = xp;
    strans(1,3) = yp;
    strans(2,3) = zp;
    
    RParameters rptmp;
    for (std::vector<fmri::OrientationMatrix>::const_iterator it = mat.begin();
         it != mat.end(); ++it) {
      mattmp = btrans * (it->Inverse() * strans);
      rptmp.xtrans = mattmp(0,3);
      rptmp.ytrans = mattmp(1,3);
      rptmp.ztrans = mattmp(2,3);            
      // Note:
      // roll = rotation around z-axis : Z
      // pitch = rotation around x-axis : X
      // yaw = rotation around y-axis : Y
      // It seems that we have YXZ?
      // useful: https://en.wikipedia.org/wiki/Euler_angles
      cy = sqrt(mattmp(0,2)*mattmp(0,2) + mattmp(2,2)*mattmp(2,2));
      if (cy > cthresh) {
        rptmp.roll = (180.0/M_PI)*atan2(mattmp(1,0),mattmp(1,1));
        rptmp.pitch = (180.0/M_PI)*atan2(-1*mattmp(1,2),cy);
        rptmp.yaw = (180.0/M_PI)*atan2(mattmp(0,2),mattmp(2,2));
      }
      else {
        rptmp.roll = (180.0/M_PI)*atan2(mattmp(1,0),mattmp(1,1));
        rptmp.pitch = (180.0/M_PI)*atan2(mattmp(0,2),cy);
        rptmp.yaw = 0.0;
      }
      rp.push_back(rptmp);
    }


  }

  /*
  void ReadMATDir(const vector<string> &filelist, vector<OrientationMatrix> &omat) {
    fstream fin;
    // int count = 0;
    string lstr;
    
    // vector<double> vals1, vals2, vals3;
    vector<double> vals;
    OrientationMatrix otmp;
    for (vector<string>::const_iterator it=filelist.begin(); it!=filelist.end(); ++it) {
      istringstream iss1, iss2, iss3;
      fin.open(*it,fstream::in);
      vals.clear();
      getline(fin,lstr);
      iss1.str(lstr);
      copy(istream_iterator<double>(iss1),istream_iterator<double>(),
	   back_inserter(vals));
      
      getline(fin,lstr);
      iss2.str(lstr);
      copy(istream_iterator<double>(iss2),istream_iterator<double>(),
	   back_inserter(vals));

      getline(fin,lstr);
      iss3.str(lstr);
      copy(istream_iterator<double>(iss3),istream_iterator<double>(),
	   back_inserter(vals));
      fin.close();

      if (vals.size() != 12) {
	return;
      }
      MatCopy(otmp,vals);
      omat.push_back(otmp);
      // ++count;
    }
  }
  */
  void WriteMAT(const std::string &filename,
		const std::vector<fmri::OrientationMatrix> &omat) {
    std::fstream fout;
    fout.open(filename,std::fstream::out);
    fmri::OrientationMatrix otmp;
    for (std::vector<fmri::OrientationMatrix>::const_iterator it=omat.begin(); 
	 it!=omat.end(); ++it) {
      MatCopyOut(*it,otmp);
      fout << otmp.StringLine() << std::endl;
    }
    fout.close();
  }
  void WriteMAT4(const std::string &filename,
		 const std::vector<fmri::OrientationMatrix> &omat) {
    std::fstream fout;
    fout.open(filename,std::fstream::out);
    fmri::OrientationMatrix otmp;
    for (std::vector<fmri::OrientationMatrix>::const_iterator it=omat.begin(); 
	 it!=omat.end(); ++it) {
      MatCopyOut(*it,otmp);
      fout << otmp.String();
    }
    fout.close();
  }

  void SetInSpace(RotationSpace rs) {inspace = rs;}
  void SetInSpaceDICOM() {inspace = RotationSpace::DICOM;}
  void SetInSpaceNIFTI() {inspace = RotationSpace::NIFTI;}
  void SetInSpaceFSL() {inspace = RotationSpace::FSL;}
  void SetInSpaceFSL(const fmri::MRIBase &source, const fmri::MRIBase &target) {
    inspace = RotationSpace::FSL;
    bool sourceflip = false;
    oms_in = source.OrientMatrix();
    omt_in = target.OrientMatrix();
    // cout << "DS " << oms_in.Determinate() << endl;
    if (oms_in.Determinate() > 0.0) {
      sourceflip = true;
    }
    oms_in.Invert();
    fmri::OrientationMatrix osscale;
    if (sourceflip) {
      osscale.Set(-1.0*source.XVoxelSize(),0.0,0.0,source.XVoxelSize()*((double)source.XSize()-1.0),
		  0.0,source.YVoxelSize(),0.0,0.0,
		  0.0,0.0,source.ZVoxelSize(),0.0);
    }
    else {
      osscale.Set(source.XVoxelSize(),0.0,0.0,0.0,
		  0.0,source.YVoxelSize(),0.0,0.0,
		  0.0,0.0,source.ZVoxelSize(),0.0);
    }
    oms_in = osscale * oms_in;
    // cout << "DT " << omt_in_tr.Determinate() << endl;
    fmri::OrientationMatrix otscale;
    if (omt_in.Determinate() > 0.0) {      
      otscale.Set(-1.0/target.XVoxelSize(),0.0,0.0,(double)target.XSize()-1.0,
		    0.0,1.0/target.YVoxelSize(),0.0,0.0,
		    0.0,0.0,1.0/target.ZVoxelSize(),0.0);
    }
    else {
      otscale.Set(1.0/target.XVoxelSize(),0.0,0.0,0.0,
		    0.0,1.0/target.YVoxelSize(),0.0,0.0,
		    0.0,0.0,1.0/target.ZVoxelSize(),0.0);
    }
    omt_in = omt_in * otscale;
    // omtarget = omtarget * otscale;


  }
  void SetOutSpace(RotationSpace rs) {outspace = rs;}
  void SetOutSpaceDICOM() {outspace = RotationSpace::DICOM;}
  void SetOutSpaceNIFTI() {outspace = RotationSpace::NIFTI;}
  void SetOutSpaceFSL() {outspace = RotationSpace::FSL;}
  void SetOutSpaceFSL(const fmri::MRIBase &source, const fmri::MRIBase &target) {
    outspace = RotationSpace::FSL;
    bool targetflip = false;
    oms_out = source.OrientMatrix();
    omt_out = target.OrientMatrix();
    fmri::OrientationMatrix osscale;
    if (oms_out.Determinate() > 0.0) {
      osscale.Set(-1.0/source.XVoxelSize(),0.0,0.0,(double)source.XSize()-1.0,
		  0.0,1.0/source.YVoxelSize(),0.0,0.0,
		  0.0,0.0,1.0/source.ZVoxelSize(),0.0);
    }
    else {
      osscale.Set(1.0/source.XVoxelSize(),0.0,0.0,0.0,
		  0.0,1.0/source.YVoxelSize(),0.0,0.0,
		  0.0,0.0,1.0/source.ZVoxelSize(),0.0);
    }
    oms_out = oms_out * osscale;
    // cout << "DT " << omt_in_tr.Determinate() << endl;

    if (omt_out.Determinate() > 0.0) {
      targetflip = true;
    }
    omt_out.Invert();
    fmri::OrientationMatrix otscale;
    if (targetflip) {      
      otscale.Set(-1.0*target.XVoxelSize(),0.0,0.0,target.XVoxelSize()*((double)target.XSize()-1.0),
		  0.0,target.YVoxelSize(),0.0,0.0,
		  0.0,0.0,target.ZVoxelSize(),0.0);
    }
    else {
      otscale.Set(target.XVoxelSize(),0.0,0.0,0.0,
		  0.0,target.YVoxelSize(),0.0,0.0,
		  0.0,0.0,target.ZVoxelSize(),0.0);
    }
    omt_out = otscale * omt_out;
    // omtarget = omtarget * otscale;
  }
  
  void ApplyInverse(bool val) {applyinv = val;}

private:
  void MatCopy(fmri::OrientationMatrix &omat, const std::vector<double> &vals) {
    if (inspace == RotationSpace::DICOM) {
      // AFNI rotation matrices are stored in dicom
      // coordinate space. To convert a NIFTI to dicom
      // multiply x and y by -1. We can convert a
      // dicom rotation matrix to nifti by multiplying
      // the matrix (M) by diag([-1 -1 1 1])*M*diag([-1 -1 1 1])
      // which is
      // [A B C D       [A  B -C -D
      //  E F G H  -->   E  F -G -H
      //  I J K L       -I -J  K  L
      //  0 0 0 1]       0  0  0  1]
      omat.Set(vals[0],vals[1],-1*vals[2],-1*vals[3],
	       vals[4],vals[5],-1*vals[6],-1*vals[7],
	       -1*vals[8],-1*vals[9],vals[10],vals[11]);
    }
    else if (inspace == RotationSpace::FSL) {
      fmri::OrientationMatrix otmp(vals[0],vals[1],vals[2],vals[3],
	       vals[4],vals[5],vals[6],vals[7],
	       vals[8],vals[9],vals[10],vals[11]);
      omat = omt_in * (otmp * oms_in);
    }
    else {
      omat.Set(vals[0],vals[1],vals[2],vals[3],
	       vals[4],vals[5],vals[6],vals[7],
	       vals[8],vals[9],vals[10],vals[11]);   

    }
    if (applyinv) {
      // cout << omat.String() << endl;
      omat.Invert();
      // cout << omat.String() << endl;
    }
  }
  void MatCopyOut(const fmri::OrientationMatrix &omatin,
		  fmri::OrientationMatrix &omatout) {
    if (outspace == RotationSpace::DICOM) {
      omatout.Set(omatin(0,0),omatin(0,1),-1*omatin(0,2),-1*omatin(0,3),
		  omatin(1,0),omatin(1,1),-1*omatin(1,2),-1*omatin(1,3),
		  -1*omatin(2,0),-1*omatin(2,1),omatin(2,2),omatin(2,3));

    }
    else if (outspace == RotationSpace::FSL) {
      
      omatout = omt_out * (omatin * oms_out);
    }
    else {
      omatout = omatin;
    }
  }

  RotationType rtype;
  RotationSpace inspace;
  RotationSpace outspace;
  fmri::OrientationMatrix oms_in;
  fmri::OrientationMatrix omt_in;
  fmri::OrientationMatrix oms_out;
  fmri::OrientationMatrix omt_out;
  bool applyinv;
  double FOCThresh;
};


#endif
