#include <iostream>
#include <cmath>
#include "polykregress.h"


inline void dfill(double *X, int N, double a) {
  for (int ii=0; ii<N; ++ii) {
    X[ii] = a;
  }
}



void polykregress(const double *X, int M, int N, int L, 
		  const double *Y, int Q, double *Yh, double *df,
		  const double *sigma, bool matched, int skip) {

  // * Input *
  // X is M x N x L matrix
  // Y is M x Q matrix
  // sigma is 1 x N matrix
  // Note: skip will skip the first values along the M dimension
  // * Output *
  // In matched mode (matched=true) L must be equal to Q
  // Yh is M x Q matrix
  // df is 2 x Q matrix
  // otherwise
  // Yh is M x Q x L matrix
  // df is 2 x Q x L matrix

  // This routine is designed to run many kernel regressions at
  // once. A different regression kernel is estimated for each matrix
  // in X along the L dimension. A solution is calculated for each
  // column of Y. In matched mode the matrices in X are matched to the
  // columns in Y and one analysis is done for each. Otherwise, each
  // matrix in X is run for every column in Y giving L*Q analyses.

  // df (degrees of freedom) has two values on output.
  //   1: trace(W)
  //   2: trace(W'*W)
  // Where W is the normalized weight matrix used in the kernel regression
  // ie, yhat = W*y
  // The variance of the output data accounting for the lost degrees of
  // freedom is sig^2 * (N - 2*trace(W) + trace(W'*W))

  int ii, jj, kk, ll;
  double *kernel, *kernelsq;
  double tval1, tval2;

  if (matched) {
    dfill(Yh,M*Q,0.0);
    dfill(df,2*Q,0.0);
  }
  else {
    dfill(Yh,M*Q*L,0.0);
    dfill(df,2*Q*L,0.0);
  }
#pragma omp parallel private(ii,jj,kk,ll,tval1,tval2,kernel,kernelsq)
  {
  kernel = new double[M];
  kernelsq = new double [M];
  
#pragma omp for
  for (ii=0; ii<L; ++ii) { // Loop through X matrices
    dfill(kernel,M,0.0);
    dfill(kernelsq,M,0.0);
    for (jj=skip; jj<M; ++jj) {
      for (kk=jj; kk<M; ++kk) {
	if (jj==kk) {
	  if (matched) {
	    Yh[ii*M+jj] += Y[ii*M+jj];
	  }
	  else {
	    for (ll=0; ll<Q; ++ll) {
	      Yh[ii*M*Q+ll*M+jj] += Y[ii*M+jj];
	    }
	  }
	  kernel[jj] += 1.0;
	  kernelsq[jj] += 1.0;
	}
	else {
	  tval2 = 0.0;
	  for (ll=0; ll<N; ++ll) {
	    tval1 = X[ii*M*N + ll*M + kk] - X[ii*M*N + ll*M + jj];
	    tval2 += (tval1*tval1) / (2*sigma[ll]*sigma[ll]);
	  }
	  tval2 = exp(-1*tval2);
	  if (matched) {
	    Yh[ii*M+jj] += tval2*Y[ii*M+kk];
	    Yh[ii*M+kk] += tval2*Y[ii*M+jj];
	  }
	  else {
	    for (ll=0; ll<Q; ++ll) {
	      Yh[ii*M*Q+ll*M+jj] += tval2*Y[ii*M+kk];
	      Yh[ii*M*Q+ll*M+kk] += tval2*Y[ii*M+jj];
	    }
	  }
	  kernel[jj] += tval2;
	  kernel[kk] += tval2;
	  kernelsq[jj] += (tval2*tval2);
	  kernelsq[kk] += (tval2*tval2);
	}

      }
      if (matched) {
	Yh[ii*M+jj] /= kernel[jj];
	// df[ii] += 1.0/kernel[jj];
	df[2*ii] += 1.0/kernel[jj];
	df[2*ii+1] += kernelsq[jj]/(kernel[jj]*kernel[jj]);
      }
      else {
	for (ll=0; ll<Q; ++ll) {
	  Yh[ii*M*Q+ll*M+jj] /= kernel[jj];
	  // df[ii*Q+ll] += 1.0/kernel[jj];
	  df[2*(ii*Q+ll)] += 1.0/kernel[jj];
	  df[2*(ii*Q+ll)+1] += kernelsq[jj]/(kernel[jj]*kernel[jj]);
	}
      }
    }
  }

  delete [] kernel;
  delete [] kernelsq;
  }
}
