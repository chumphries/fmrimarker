/*********************************************************************
* Copyright 2017, Colin Humphries
*
* This file is part of FMRIMarker.
*
* FMRIMarker is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* FMRIMarker is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with FMRIMarker.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
/*!
  \file marker.h
  \author Colin Humphries
  \brief Class for running markr analysis.
*/
#ifndef _MARKER_H
#define _MARKER_H

#include <vector>
#include <cmath>
#include <cassert>
#include <iostream>
#include "mrivol.h"
#include "polykregress.h"

// using namespace std;

/*! \class Marker
  \brief Class for modeling movement artifact in fMRI data with kernel regression.



  \author Colin Humphries
*/
class Marker {
public:
  /*!
    Class constructor.
  */
  Marker() {
    ssigma = 0.1; // mm
    tsigma = -1.0; // datapoints (not sec!)
    sorder = 1;
    torder = 1;
    skip = 2;
    X = nullptr;
    debug = false;
  }
  ~Marker() {
    if (X) {
      delete [] X;
    }
  }
  /*!
    Run marker analysis.
    \param[in] data fMRI timecourse
    \param[out] model estimated timecourse of movement
    \param[in] matdata rotation matrices
   */
  void Analyze(fmri::MRIData<double> &data, fmri::MRIData<double> &model, 
	       const std::vector<fmri::OrientationMatrix> &matdata) {
    // Note: this should eventually be deleted.
    size_t M = data.NumTimePnts();
    size_t N = 4;
    size_t L = data.NumVoxels();
    if (debug) {
      std::cout << "M " << M << std::endl;
      std::cout << "L " << L << std::endl;
      std::cout << matdata.size() << std::endl;
    }
    // double xp, yp, zp, nxp, nyp, nzp;
    double *sigma = new double[N];
    int *order = new int[N];
    sorder = 0;
    sigma[0] = ssigma;
    sigma[1] = ssigma;
    sigma[2] = ssigma;
    sigma[3] = tsigma;
    order[0] = sorder;
    order[1] = sorder;
    order[2] = sorder;
    order[3] = torder;
      
    SetPositions(data,matdata);
    /*
    polykregress(X,M,N,L,data.data_ptr(),L,
		 model.data_ptr(),sigma,order,true,skip);
    */
    delete [] sigma;
    delete [] order;
  }
  /*!
    Run marker analysis.
    \param[in] data fMRI timecourse
    \param[out] model estimated timecourse of movement
   */
  void Analyze(fmri::MRIData<double> &data, fmri::MRIData<double> &model,
	       fmri::MRIData<double> &df) {

    size_t M = data.NumTimePnts();
    size_t N = 4;
    size_t L = data.NumVoxels();
    if (debug) {
      std::cout << "M " << M << std::endl;
      std::cout << "L " << L << std::endl;
    }
    assert(X);
    assert(Xsize == M*N*L);
    assert(L == model.NumVoxels());
    assert(M == model.NumTimePnts());
    assert(L == df.NumVoxels());

    double *sigma = new double[N];
    // sorder = 0;
    sigma[0] = ssigma;
    sigma[1] = ssigma;
    sigma[2] = ssigma;
    sigma[3] = tsigma;
    // order[0] = sorder;
    // order[1] = sorder;
    // order[2] = sorder;
    // order[3] = torder;
      
    polykregress(X,M,N,L,data.data_ptr(),L,
		 model.data_ptr(),df.data_ptr(),
		 sigma,true,skip);
    
    delete [] sigma;
    // delete [] order;
  }
  /*!
    Calculate residuals i.e., cleaned data
    \param[in,out] data fMRI timecourse (input), cleaned data (output)
    \param[in] model from Markr::Analyze
  */
  void ResidualSkip(const fmri::MRIData<double> &data, fmri::MRIData<double> &model) {
    // In this function the results are returned in the variable data.
    // The ignored images at the beginning are left as is.
    size_t ii, jj;
    // #pragma omp parallel for
    for (ii=0; ii<data.NumTimePnts(); ++ii) {
      for (jj=0; jj < data.NumVoxels(); ++jj) {
	if (ii<skip) {
	  model(ii,jj) = data(ii,jj);
	}
	else {
	  model(ii,jj) = data(ii,jj) - model(ii,jj);
	}
      }
    }
  }
  /*!
    Calculate residuals i.e., cleaned data
    \param[in,out] data fMRI timecourse (input), cleaned data (output)
    \param[in] model from Markr::Analyze
  */
  void ResidualZero(const fmri::MRIData<double> &data, fmri::MRIData<double> &model) {
    // This function is similar to ResidualSkip, however the ignored images
    // at the beginning are set to 0.0
    size_t ii, jj;
    // #pragma omp parallel for
    for (ii=0; ii<data.NumTimePnts(); ++ii) {
      for (jj=0; jj < data.NumVoxels(); ++jj) {
	if (ii<skip) {
	  model(ii,jj) = 0.0;
	}
	else {
	  model(ii,jj) = data(ii,jj) - model(ii,jj);
	}
      }
    }
  }
  /*!
    Calculate model r-squared values
    \param[in] data fMRI timecourse (input)
    \param[in] model from Markr::Analyze
    \param[out] stats resulting r-square values
  */
  void RSquared(const fmri::MRIData<double> &data, const fmri::MRIData<double> &model,
		fmri::MRIData<double> &stats) {
    // Note that stats should have 6 subvolumes
    double M, S1, S2, S3, C;
    for (size_t ii=0; ii<data.NumVoxels(); ++ii) {
      M = 0.0;
      S1 = 0.0; // SS total
      S2 = 0.0; // SS model
      S3 = 0.0; // SS residuals
      C = 0.0;
      // Calculate mean
      for (size_t jj=0; jj < data.NumTimePnts(); ++jj) {
	if (jj >= skip) {
	  M += data(jj,ii);
	}
      }
      M /= (double)(data.NumTimePnts() - skip);
      for (size_t jj=0; jj < data.NumTimePnts(); ++jj) {
	if (jj >= skip) {
	  S1 += (data(jj,ii)-M)*(data(jj,ii)-M);
	  S2 += (model(jj,ii)-M)*(model(jj,ii)-M);
	  S3 += (data(jj,ii)-model(ii,jj))*(data(jj,ii)-model(ii,jj));
	  C += (data(jj,ii)-M)*(model(jj,ii)-M);
	}
      }
      if (S1*S2 > 0.0) {
	stats(0,ii) = (C*C) / (S1*S2);
      }
      else {
	stats(0,ii) = 0.0;
      }
      stats(3,ii) = S1;
      stats(4,ii) = S3;
      stats(5,ii) = S2;
    }
  }
  /*!
    Calculate voxel positions at each time point
    \param[in] data fMRI timecourse (input)
    \param[in] matdata vector of orientation matrices
  */  
  void SetPositions(const fmri::MRIData<double> &data,
		    const std::vector<fmri::OrientationMatrix> &matdata) {
    size_t M = data.NumTimePnts();
    size_t N = 4;
    size_t L = data.NumVoxels();
    if (X) {
      if (Xsize != M*N*L) {
	delete [] X;
	X = new double[M*N*L];
	Xsize = M*N*L;
      }
    }
    else {
      X = new double[M*N*L];
      Xsize = M*N*L;
    }
    double xp, yp, zp, nxp, nyp, nzp;
    for (size_t ii=0; ii<L; ++ii) {
      data.Vox2Pos(ii,xp,yp,zp);
      for (size_t jj=0; jj<M; ++jj) {
	matdata[jj].Transform(xp,yp,zp,nxp,nyp,nzp);
	X[jj+0*M+ii*M*N] = nxp;
	X[jj+1*M+ii*M*N] = nyp;
	X[jj+2*M+ii*M*N] = nzp;
	X[jj+3*M+ii*M*N] = jj;	
      }
      if ((ii==0) & debug) {
	std::cout << "Voxel 0" << std::endl;
	std::cout << "Original position = " << xp << " " << yp << " " << zp << std::endl;
	for (uint jj=0; jj<M; ++jj) {
	  std::cout << jj << ": " << X[jj] << " " << X[jj+M] << " " << X[jj+2*M] << std::endl;
	}
      }
    }
  }
  /*!
    Set movement kernel width (mm).
    \param fwhm full width half maximum of kernel
   */
  void SetFWHM(double fwhm) {
      ssigma = fwhm * 0.424660891294;
  }
  /*!
    Set movement polynomial order.
    \param ord
   */
  void SetOrder(int ord) {
      sorder = ord;
  }
   /*!
    Set temporal kernel width (# volumes).
    \param fwhm full width half maximum of kernel
   */
  void SetTrendFWHM(double fwhm) {
      tsigma = fwhm * 0.424660891294;
  }
   /*!
    Set temporal order.
    \param ord 
   */
  void SetTrendOrder(int ord) {
      torder = ord;
  }
  /*!
    Set number of volumes to skip at beginning.
    \param numvols number of volumes
   */
  void SetSkip(size_t numvols) {skip = numvols;}
  /*!
    Turn debug output on/off.
    \param sdb
   */  
  void SetDebug(bool sdb) {debug = sdb;}

private:
  double ssigma;
  double tsigma;
  size_t skip;
  int sorder;
  int torder;
  double *X;
  size_t Xsize;
  bool debug;
};


#endif
